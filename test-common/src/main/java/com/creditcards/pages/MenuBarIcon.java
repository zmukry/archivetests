package com.creditcards.pages;

public interface MenuBarIcon extends PageObject{
    void click();
}