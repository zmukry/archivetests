package com.creditcards.pages;

import org.openqa.selenium.WebElement;

public interface NavigationMenu extends PageObject{
    public NavigationTab getSelectedNavigationTab();

    public boolean isTabSelected(NavigationTab navigationTab);

    public void clickTab(NavigationTab navigationTab);

    WebElement selectedTab();

    WebElement navigationMenu();
}
