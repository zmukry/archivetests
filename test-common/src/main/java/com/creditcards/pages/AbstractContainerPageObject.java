package com.creditcards.pages;

import org.jbehave.web.selenium.WebDriverProvider;

public abstract class AbstractContainerPageObject extends AbstractPageObject implements ContainerPageObject{

    public AbstractContainerPageObject(WebDriverProvider webDriverProvider){
        super(webDriverProvider);
    }

    @Override
    public void initElements(){
        super.initElements();
        initPageElements();
    }
}
