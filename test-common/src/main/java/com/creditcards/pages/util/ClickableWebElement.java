package com.creditcards.pages.util;

import org.openqa.selenium.WebElement;

public class ClickableWebElement implements ActionableWebElement{

    private final WebElement clickableWebElement;

    public ClickableWebElement(WebElement clickableWebElement){
        this.clickableWebElement = clickableWebElement;
    }

    public void invoke(){
        this.clickableWebElement.click();
    }
}
