package com.creditcards.pages.util;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;

public class SpaceBarInvokableWebElement implements ActionableWebElement{
    private final WebElement spaceBarInvokableWebElement;

    public SpaceBarInvokableWebElement(WebElement spaceBarInvokableWebElement){
        this.spaceBarInvokableWebElement = spaceBarInvokableWebElement;
    }

    public void invoke(){
        spaceBarInvokableWebElement.sendKeys(Keys.SPACE);
    }
}
