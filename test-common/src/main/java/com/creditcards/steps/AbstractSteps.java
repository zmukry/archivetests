package com.creditcards.steps;

import com.creditcards.pages.PageObject;
import com.creditcards.util.Selection;
import com.creditcards.util.SelectionConverter;
import com.creditcards.util.Visibility;
import com.creditcards.util.VisibilityConverter;
import org.jbehave.core.annotations.AsParameterConverter;
import org.jbehave.web.selenium.WebDriverProvider;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;

@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public abstract class AbstractSteps{
    private final WebDriverProvider webDriverProvider;

    public AbstractSteps(WebDriverProvider driverProvider){
        this.webDriverProvider = driverProvider;
    }

    protected WebDriverProvider getDriverProvider(){
        return webDriverProvider;
    }

    public abstract PageObject getPageObject();

    public abstract void setWebPageObject();

    @AsParameterConverter
    public Visibility visibleState(String value){
        return (Visibility)new VisibilityConverter().convertValue(value,Visibility.class);
    }

    @AsParameterConverter
    public Selection selectedState(String value){
        return (Selection)new SelectionConverter().convertValue(value,Selection.class);
    }
}