package com.creditcards.service;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;

public class MessageService{

    @Value("#{settings['test.domain']}")
    private String locale;

    private MessageSource messageSource;

    protected MessageService(){
    }

    public String valueOf(String key){
        return messageSource.getMessage(key,new Object[0],null);
    }
}