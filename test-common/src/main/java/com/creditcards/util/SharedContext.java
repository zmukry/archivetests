package com.creditcards.util;

import org.springframework.stereotype.Component;

import java.util.concurrent.ConcurrentHashMap;

/**
 *
 */
@Component
public class SharedContext extends ConcurrentHashMap<Object, Object>{
    private static final long serialVersionUID = 0L;
}