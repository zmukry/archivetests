package com.creditcards.util;

import org.apache.commons.lang3.StringUtils;

public enum Visibility{
    DISPLAYED("displayed",true),
    HIDDEN("hidden",false);

    private final boolean visible;
    private final String state;

    Visibility(String name, boolean display){
        this.visible = display;
        this.state = name;
    }

    public boolean isDisplayed(){
        return this.visible;
    }

    public static Visibility valueOfByState(String display){
        Visibility result = null;
        for( Visibility component : Visibility.values() ){
            if( StringUtils.equalsIgnoreCase(display,component.state) ){
                result = component;
                break;
            }
        }
        return result;
    }

    public boolean equalsValue(boolean isVisible){
        return this.visible == isVisible;
    }
}
