package com.creditcards.cache.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

/*
* Want
* To verify that the Shared Context Service is Multi-threaded & maintains its state.
 */
@ContextConfiguration(locations = "classpath:applicationContext.xml")
public class CacheServiceTest extends AbstractTestNGSpringContextTests{

    @Autowired
    private ThreadPoolTaskExecutor taskExecutor;

    @Autowired
    private CacheService cacheService;

    private enum Keys{
        PAGE,
        LINE,
        APP
    }

    @BeforeMethod
    public void setUp(){
        logger.info("Pre setting cace Shared Context");
        cacheService.put(Keys.APP,"ccx");
        cacheService.put(Keys.PAGE,"Card");
        cacheService.put(Keys.LINE,"123");
    }

    @Test
    public void testCacheService(){
        assertEquals((String)cacheService.get(Keys.APP),"ccx","Verifing the APP Key value.");
        assertEquals((String)cacheService.get(Keys.PAGE),"Card","Verifing the Page Key value.");
        assertEquals((String)cacheService.get(Keys.LINE),"123","Verifing the Line Key value.");
    }

    @Test
    public void testMultiThread(){
        for( int i = 0;i < 10;i++ ){
            taskExecutor.submit(new Airplane("L" + i));
        }
        taskExecutor.shutdown();
        for( int i = 0;i < 10;i++ ){
            Airplane plane = (Airplane)cacheService.get("L" + i);
            logger.info(plane.getName());
        }
    }

    public final class Airplane implements Runnable{
        private String name;

        public Airplane(String aFlightId){
            fFlightId = aFlightId;
            cacheService.put(fFlightId,this);
        }

        @Override
        public void run(){
            Thread.currentThread().setName(fFlightId);
            takeOff();
        }

        public String getName(){
            return this.name;
        }

        private final String fFlightId;

        private void takeOff(){
            name = fFlightId;
        }
    }
}