package com.creditcards.webdriver;

import org.testng.annotations.Test;

import java.net.InetAddress;

public class HostNameTest{

    @Test
    public void verifyHostName() throws Exception{
        final InetAddress localHost = InetAddress.getLocalHost();
        System.out.println("hostAddress: " + localHost.getHostAddress());
        System.out.println("hostName: " + localHost.getHostName());
    }
}
