package com.creditcards.webdriver;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

@ContextConfiguration(locations = "classpath:applicationContext.xml" )
public class AppContextConfigurationTest extends AbstractTestNGSpringContextTests{
    private  static final Logger logger = LoggerFactory.getLogger(AppContextConfigurationTest.class);

    @Value("#{settings['locale']}")
    private String locale;

    @Value("#{settings['browser']}")
    private String browser;

    @Test
    public void testIsSeleniumActive(){
        logger.info("browser VALUE:" + browser);
        logger.info("Locale VALUE:" + locale);
        assertEquals(browser,"FIREFOX");
        assertEquals(locale,"en_US");
    }
}

