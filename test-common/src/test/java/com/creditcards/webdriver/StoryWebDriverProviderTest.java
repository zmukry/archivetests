package com.creditcards.webdriver;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.Test;

@ContextConfiguration(locations = { "classpath:jbehave-test-configuration.xml","classpath:selenium-test.xml" })

public class StoryWebDriverProviderTest extends AbstractTestNGSpringContextTests{
    private final static Logger logger = LoggerFactory.getLogger(StoryWebDriverProviderTest.class);

    @Autowired
    private StoryWebDriverProvider storyWebDriverProvider;

    @Test
    public void testIsSeleniumActive(){
        logger.info("Testing New Selenium WebDriver ProviderConfiguration");
    }
}