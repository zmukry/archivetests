package com.creditcards.webdriver;

import com.creditcards.test.runner.TestStoryFinder;
import org.jbehave.core.InjectableEmbedder;
import org.jbehave.core.annotations.Configure;
import org.jbehave.core.annotations.UsingEmbedder;
import org.jbehave.core.annotations.spring.UsingSpring;
import org.jbehave.core.configuration.Configuration;
import org.jbehave.core.embedder.Embedder;
import org.jbehave.core.embedder.StoryControls;
import org.jbehave.core.failures.FailingUponPendingStep;
import org.jbehave.core.io.LoadFromClasspath;
import org.jbehave.core.junit.spring.SpringAnnotatedEmbedderRunner;
import org.jbehave.core.steps.ParameterConverters;
import org.jbehave.web.selenium.SeleniumConfiguration;
import org.jbehave.web.selenium.SeleniumContext;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

import static com.google.common.collect.Lists.newArrayList;

@RunWith(SpringAnnotatedEmbedderRunner.class)

@Configure(using = SeleniumConfiguration.class,
           pendingStepStrategy = FailingUponPendingStep.class,
           parameterConverters = ParameterConverters.EnumConverter.class)

@UsingEmbedder(embedder = Embedder.class,generateViewAfterStories = true,ignoreFailureInStories = true,
               ignoreFailureInView = false,
               storyTimeoutInSecs = 100,
               threads = 1,
               metaFilters = "-skip")
@UsingSpring(resources = { "jbehave-test-configuration.xml","selenium-test.xml" })
public class StoryWebDriverEmbedderRunner extends InjectableEmbedder{
    private final TestStoryFinder storyFinder = new TestStoryFinder();
    @Autowired
    private StoryWebDriverProvider webDriverProvider;

    @Test
    public void run() throws Throwable{
        Configuration configuration = injectedEmbedder().configuration();
        configuration.useFailureStrategy(new FailingUponPendingStep())
                     .useStoryControls(new StoryControls().doResetStateBeforeScenario(false))
                     .useStoryLoader(new LoadFromClasspath(StoryWebDriverEmbedderRunner.class));
        SeleniumConfiguration seleniumConfiguration = (SeleniumConfiguration)configuration;
        seleniumConfiguration.useWebDriverProvider(webDriverProvider);
        injectedEmbedder().runStoriesAsPaths(storyFinder.storyPaths(getIncludes()));
    }

    public void _run2(){
        SeleniumContext seleniumContext = new SeleniumContext();

        Configuration configuration = injectedEmbedder().configuration();
        SeleniumConfiguration seleniumConfiguration = (SeleniumConfiguration)configuration;
        seleniumConfiguration.useSeleniumContext(seleniumContext);
        seleniumConfiguration.useWebDriverProvider(webDriverProvider);

        injectedEmbedder().runStoriesAsPaths(storyFinder.storyPaths(getIncludes()));
    }

    private List<String> getIncludes(){
        return newArrayList("**/*.story");
    }
}
