package com.creditcards.test.pages;

import org.jbehave.web.selenium.WebDriverPage;
import org.jbehave.web.selenium.WebDriverProvider;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class EtsyWebDriverPage extends WebDriverPage{

    @FindBy(id = "etsy")
    private WebElement pageHeader;

    @FindBy(linkText = "Treasury")
    private WebElement treasuryLink;

    @Autowired
    public EtsyWebDriverPage(WebDriverProvider webDriverProvider){
        super(webDriverProvider);
    }

    public void go(){
        get("http://www.etsy.com");
    }

    public void clickTreasury(){
        final WebElement newAccountButton = findElement(By.linkText("Treasury"));
        newAccountButton.click();
    }

    public void initElements(){
        int DRIVER_WAIT = 5;
        PageFactory.initElements(new AjaxElementLocatorFactory(getDriverProvider().get(),DRIVER_WAIT),this);
    }
}