package com.creditcards.test.steps;

public class TradingService{

    public Stock newStock(String symbol, double threshold){
        return new Stock(symbol,threshold);
    }

    public Trader newTrader(String name, String rank){
        return new Trader(name,rank);
    }
}
