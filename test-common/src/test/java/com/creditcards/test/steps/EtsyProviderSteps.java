package com.creditcards.test.steps;

import com.creditcards.steps.Steps;
import com.creditcards.test.pages.EtsyWebDriverPage;
import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.jbehave.web.selenium.WebDriverProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

@Steps
public class EtsyProviderSteps{
    private final static Logger logger = LoggerFactory.getLogger(EtsyProviderSteps.class);

    private final WebDriverProvider webDriverProvider;

    @Autowired
    EtsyWebDriverPage etsyWebDriverPage;

    @Autowired
    public EtsyProviderSteps(WebDriverProvider webDriverProvider){
        this.webDriverProvider = webDriverProvider;
    }

    @Given("I am on etsy.com")
    public void homepageOnEtsyDotCom(){
        etsyWebDriverPage.go();
    }

    @Given("I am searching on Etsy.com")
    public void advancedSearchingOnEtsyDotCom(){
        logger.info("GIVEN");
    }

    @When("I want to browse the treasury")
    public void browseTreasury(){
        logger.info("GIVEN");
    }

    @Then("results will be displayed in the gallery")
    public void thereAreSearchResults(){
        logger.info("GIVEN");
    }
}
