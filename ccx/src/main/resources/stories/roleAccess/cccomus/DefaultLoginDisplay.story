Narrative:
As either a sales or non-sales creditcards.com adminstrtator
I want to Log into CCX
So that I can verify the default initial display is correct

Scenario: Logging into ccx as a creditcards.com Sales System Administrator.

Given I am User assigned to the Issuer <issuer>
And I am a User assigned the Role <role>

When I login as <username>
Then I am on the Overview page
And the Page Header is displayed
And the Page Sub Header options are displayed
And the Card Introduction is displayed
And the Product List is displayed
And the Page Footer is displayed

When the Page Header is displayed
Then the CCX Logo is displayed
Then the Welcome Message is displayed
And the Issuer Logo is displayed
And the Main Navigation Bar is displayed

When the Welcome Message is displayed
Then the Welcome Message contains the users name and role
And the Logout link is displayed

When the Page Sub Header options are displayed
Then the Card Navigation options are displayed
And the Emulate Issuer selection is displayed
And the Search Card List filter is displayed

When the Card Navigation options are displayed
Then the following Card Navigation options are displayed:
|nav-cards          |
|Add New Card       |
|Hide Offline       |
|Overview           |
|Print Card List    |

When the Emulate Issuer selection is displayed
Then there is no selected issuer being emulated

When the Search Card List filter is displayed
Then the Search Card List filter contains the text Search Card List

When the Card Introduction is displayed
Then the Card Introduction conatins the text cardinto.txt

When there is no selected emulated issuer
Then the Product List will display "No records found"

When the Page Footer is displayed
Then the Page Footer copyright is displayed
And the Page Footer logo is displayed
And the Page Footer Security Policy link is displayed

Examples:
|username       |issuer             |role                   |name   |
|QA_SystemAdmin |creditcards.com    |Systems Administrator  |Bugs   |