A CCX Internal User is defined as:
    1. Assigned only to the creditcards.com issuer
    2. Assigned only the System or Account administrative roles
    3. Are required to emulate an issuer because initially no issuer is selected.

Meta:

Narrative:
As either a sales or non-sales creditcards.com adminstrtator
I want to Log into CCX
So that I can verify which main navigational tabs are displayed and accessiable

Scenario: Verify the Main Navigational Tabs for the internal administrator accounts
Given I am User assigned to the Issuer <issuer>
And I am a User assigned the Role <role>
When I login
And I am on the Overview page
Then the Page Header is displayed
And the Page Sub Header options are displayed
And the Card Introduction is displayed
And the Product List is displayed
And the Page Footer is displayed

When the Page Header is displayed
Then the CCX Logo is displayed
And the Welcome Message is displayed
And the Issuer Logo is displayed
And the Main Navigation Bar is displayed

When the Welcome Message is displayed
Then the Welcome Message contains the users name and role
And the Logout link is displayed

When the Main Navigation Bar is displayed
Then the Main Navigation Bar contains the tabs:
|tab_name                   |
|XML Recipients             |
|CreditCards.com Affiliates |
|Affiliates w/o Sites       |
|Reporting                  |
|Card Bidding Tool          |

When the Page Sub Header options are displayed
Then the Card Navigation options are displayed
And the Search Card List filter is displayed
And the Emulate Issuer selection is displayed

When the Card Navigation options are displayed
Then the following Card Navigation options are displayed:
|nav-cards          |
|Add New Card       |
|Hide Offline       |
|Overview           |
|Print Card List    |

When the Emulate Issuer selection is displayed
Then there is no selected emulated issuer

When there is no selected emulated issuer
Then the Product List will display "No records found"

Examples:
|role                   |issuer             |
|Systems Administrator  |CreditCards.com    |
|Account Admin          |CreditCards.com    |