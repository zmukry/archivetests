Meta:

Narrative:
As an External Bank Of America user
I want to Log into CCX
So that I can verify the Reporting Navigational Tab is displayed and accessiable.

Scenario: Verify Reporting access for Bank of America Administrator Account users.
Given I am assigned to the Issuer <issuer>
And I am a user assigned the role <role>
When I login
Then I am on the Overview page
And the Page Header is displayed
And the Page Sub Header options are displayed
And the Card Introduction is displayed
And the Product List is displayed
And the Page Footer is displayed

When the Page Header is displayed
Then the CCX Logo is displayed
And the Welcome Message is displayed
And the Issuer Logo is displayed
And the Main Navigation Bar is displayed

When the Welcome Message is displayed
Then the Welcome Message contains the users name and role
And the Logout link is displayed

When the Main Navigation Bar is displayed
Then the Main Navigation Bar contains the tabs:
|tab_name       |
|Cards          |
|XML Recipients |
|Reporting      |

When the Page Sub Header options are displayed
Then the Card Navigation options are displayed
And the Search Card List filter is displayed
And the Emulate Issuer selection is hidden

When the Card Navigation options are displayed
Then the following Card Navigation options are displayed
|nav-cards          |
|Add New Card       |
|Overview           |
|Print Card List    |

When the Product List is displayed
Then the Product List includes only active products
And the displayed product data coincides with the product data in the file boa/products.csv

Examples:
|role               |issuer             |
|Account Manager    |First PREMIER Bank |
|Account User       |First PREMIER Bank |


Scenario: Verify the Main Navigational Tabs for the Bank of America Card Bidding Tool Account Users.
Given I am assigned to the Issuer <issuer>
And I am a user assigned the role <role>
When I login
Then I am on the Overview page
And the Page Header is displayed
And the Page Sub Header options are displayed
And the Card Introduction is displayed
And the Product List is displayed
And the Page Footer is displayed

When the Page Header is displayed
Then the CCX Logo is displayed
And the Welcome Message is displayed
And the Issuer Logo is displayed
And the Main Navigation Bar is displayed

When the Welcome Message is displayed
Then the Welcome Message contains the users name and role
And the Logout link is displayed

When the Main Navigation Bar is displayed
Then the Main Navigation Bar contains the tabs:
|tab_name           |
|Cards              |
|XML Recipients     |
|Reporting          |
|Card Bidding Tool  |

When the Page Sub Header options are displayed
Then the Card Navigation options are displayed
And the Search Card List filter is displayed
And the Emulate Issuer selection is hidden

When the Card Navigation options are displayed
Then the following Card Navigation options are displayed
|nav-cards          |
|Add New Card       |
|Overview           |
|Print Card List    |

When the Product List is displayed
Then the Product List includes only active products
And the displayed product data coincides with the product data in the file boa-products.csv

Examples:
|role               |issuer          |
|CPBT Bid Monitor   |Bank of America |
|Issuer Bidder      |Bank of America |
|Issuer Bid Viewer  |Bank of America |