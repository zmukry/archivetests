
Meta:

Narrative:
As an Internal user
I want to Log into CCX and open a report dashboard page
So that I can view the perfromance dailiy and monthly details for any emulated issuer.

Scenario: Able to access and view the Report Dashboard page for the emulated Issuer MetaBank

Given I login as <username>
And I am on the Overview page
And the Page Header is displayed
And the Page Sub Header options are displayed

When the Card Navigation options are displayed
And the Emulate Issuer selection is displayed
Then I emulate the issuer MetaBank
And the Loading Dialog page is displayed

When the Loading Dialog page is hidden
Then the Product List is displayed

When the Main Navigation Bar is displayed
And I click the Main Navigation Bar Reporting tab
Then the Report Dashboard page is displayed
And the report Performance Bar is displayed
And the report Dashboard Report is displayed
And the report Top Cards by Accounts is displayed
And the report Top Sites by Accounts is displayed


Scenario: Verifing the Monthly Performance Details for the emulated Issuer MetaBank
Given I login as <username>
And I am on the Report Dashboard page

When the report Performance Bar is displayed
Then the report Performance Bar title displays the current_month
And the Performance Bar metrics are:
|title           |image             |value  |
|NEW WEBSITES    |newwebsites.png   |3      |
|ACTIVE WEBSITES |activewebsites.png|2      |
|TOTAL CLICKS    |totalclicks.png   |7      |
|TOTAL ACCOUNTS  |totalact.png      |861    |


Scenario: Verifing the Dashboard Reports for the emulated Issuer MetaBank
Given I login as <username>
And I am on the Report Dashboard page

When the report Dashboard Report is displayed
Then the report Dashboard Report report selection includes the reports:
|report_name                |
|Clicks By Day              |
|Accounts By Click Date     |
|Accounts By Process Date   |
|Application Rate           |
|Approval Rate              |
|Conversion Rate            |
|Click Share %              |
|Account Share %            |

And the report Dashboard Report Filter Options button is displayed
And the report Dashboard Report Export Chart Data link is displayed
And the report Dashboard Report context menu is displayed
And the report Dashboard Report has a y-axis label of Number of Clicks
And the report Dashboard Report has a date range from <report_from_date> to <report_to_date>

Scenario: Verifing the Top Cards by Accounts Report for the emulated Issuer MetaBank
Given I login as <username>
And I am on the Report Dashboard page

When the report Top Cards by Accounts is displayed
Then the report Top Cards by Accounts title displays the current_month
And the report Top Cards by Accounts context menu is displayed
And the report Top Cards by Accounts legend is displayed
And the report Top Cards by Accounts piechart is displayed
And the report Top Cards by Accounts has 5 piechart divisions
And the report Top Cards by Accounts legend includes the cards:
|cardname                               |
|Account Now Gold Visa Prepaid Card_BGAL|
|Account Now Gold Visa Prepaid Card_CL  |
|AccountNow Prepaid Visa Card_C         |
|AccountNow Prepaid MasterCard_C        |
|AccountNow Gold Prepaid Card (BC)_C    |


Scenario: Verifing the Top Sites by Accounts Report for the emulated Issuer MetaBank
Given I login as <username>
And I am on the Report Dashboard page

When the report Top Sites by Accounts is displayed
Then the report Top Sites by Accounts title displays the current_month
And the report Top Sites by Accounts includes the sites:
|website_url                            |count  |percent|
|http://www.creditcards.com             |527    |100    |


Examples:
|username   |issuer     |report_from_date   |report_to_date |
|QA_Admin   |MetaBank   |Apr 20, 2013       |Jul 11, 2013   |