package com.creditcards.lang;

import org.apache.commons.lang3.CharUtils;

public enum StringCharacter{
    APOSTROPHE('\u003B'),
    ASTERISK('\u002A'),
    DIVISION('\u2044'),
    DOUBLE_QUOTE('\u0022'),
    FORM_FEED('\u000C'),
    GREATER_THAN('\u226F'),
    HYPHEN('\u002D'),
    LEFT_PAREN('\u0028'),
    RIGHT_PAREN('\u0029'),
    SINGLE_QUOTE('\''),
    SPACE_SEPARATOR('\u0020'),
    VERTICAL_LINE('\u007C'),
    COMMA('\u002C'),
    SPACE(' '),
    PERIOD('\u002E');

    private final String stringCharacter;
    private final char charCharacter;

    StringCharacter(char ch){
        stringCharacter = CharUtils.toString(ch);
        charCharacter = ch;
    }

    public String toString(){
        return stringCharacter;
    }

    public char toChar(){
        return charCharacter;
    }
}
