package com.creditcards.ccx.utils;

import org.apache.commons.lang3.StringUtils;

public enum Roles{
    ADMIN("ADMIN","Account Admin","CCCom process Admin (sales team)",true,true),
    CC_MONITOR("CC_MONITOR","CPBT Bid Monitor","CCCom process viewer (e.g. Chris or Jeff)",false,false),
    CPBT_BIDDER("CPBT_BIDDER","Issuer Bidder","Issuer process bidder (access to view and place bids)",false,false),
    CPBT_VIEWER("CPBT_VIEWER","Issuer Bid Viewer","Issuer process viewer (access to view but not place bids)",false,
                false),
    MANAGER("MANAGER","Account Manager","CCCom Account manager (sales team)",false,false),
    ROLE_SYS_ADMIN("ROLE_SYS_ADMIN","Systems Administrator","CCCom Sysadmin (non-sales)",true,true),
    USER("USER","Account User","CCCom process user (sales team)",false,false);

    private final String id;
    private final String name;
    private final String description;
    private final boolean isAdmin;
    private final boolean isInternal;

    Roles(String id, String name, String description, boolean isAdmin, boolean isInternal){
        this.id = id;
        this.name = name;
        this.description = description;
        this.isAdmin = isAdmin;
        this.isInternal = isInternal;
    }

    public String getId(){
        return id;
    }

    public String getName(){
        return name;
    }

    public String getDescription(){
        return description;
    }

    public boolean isAdmin(){
        return isAdmin;
    }

    public static Roles valueOfByName(String name){
        Roles roles = null;
        for( Roles role : Roles.values() ){
            if( StringUtils.equalsIgnoreCase(name,role.name) ){
                roles = role;
                break;
            }
        }
        return roles;
    }
}