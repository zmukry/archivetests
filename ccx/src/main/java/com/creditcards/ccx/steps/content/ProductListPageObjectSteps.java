package com.creditcards.ccx.steps.content;

import com.creditcards.ccx.pages.content.ContentPageObjects;
import com.creditcards.ccx.pages.content.ProductListPageObject;
import com.creditcards.steps.Steps;
import com.creditcards.util.Visibility;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.jbehave.web.selenium.WebDriverProvider;
import org.springframework.beans.factory.annotation.Autowired;

@Steps
public class ProductListPageObjectSteps extends PageSteps{
    ProductListPageObject productListPageObject;

    @Autowired
    public ProductListPageObjectSteps(WebDriverProvider webDriverProvider){
        super(webDriverProvider);
    }

    @Override
    public void setWebPageObject(){
        productListPageObject = (ProductListPageObject)getPageObject();
    }

    @When("the Product List is %visibleState")
    @Then("the Product List is %visibleState")
    public void isDisplayed(Visibility visibleState){
        super.isDisplayed(ContentPageObjects.PRODUCT_LIST,visibleState);
        setWebPageObject();
    }
}