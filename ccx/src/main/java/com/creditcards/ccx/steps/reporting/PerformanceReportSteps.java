package com.creditcards.ccx.steps.reporting;

import com.creditcards.ccx.pages.PageManager;
import com.creditcards.ccx.pages.converters.CurrentMonthConverter;
import com.creditcards.ccx.pages.reporting.PerformanceReports;
import com.creditcards.pages.PageObject;
import com.creditcards.steps.AbstractSteps;
import com.creditcards.util.Visibility;
import org.jbehave.core.annotations.AsParameterConverter;
import org.jbehave.web.selenium.WebDriverProvider;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;

import static org.testng.Assert.assertTrue;

public abstract class PerformanceReportSteps extends AbstractSteps{
    private PageObject pageObject;

    @Autowired
    private PageManager pageManager;

    public PerformanceReportSteps(WebDriverProvider webDriverProvider){
        super(webDriverProvider);
    }

    public PageManager getPageManager(){
        return pageManager;
    }

    @Override
    public PageObject getPageObject(){
        return pageObject;
    }

    public void visibleState(PerformanceReports performanceReports, Visibility visibleState){
        pageObject = pageManager.getPerformanceReportPageObject(performanceReports);
        switch( visibleState ){
            case DISPLAYED:
                pageObject.waitUntilLoaded();
                break;
            case HIDDEN:
                pageObject.waitUntilUnLoaded();
                break;
        }
        assertTrue(pageObject.visibleStateMatches(visibleState));
    }

    @AsParameterConverter
    public DateTime.Property currentMonth(String value){
        return (DateTime.Property)new CurrentMonthConverter().convertValue(value,String.class);
    }
}