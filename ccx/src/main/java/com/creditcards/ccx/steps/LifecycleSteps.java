package com.creditcards.ccx.steps;

import com.creditcards.ccx.pages.PageManager;
import com.creditcards.ccx.pages.application.ApplicationPages;
import com.creditcards.ccx.pages.header.HeaderPageObject;
import com.creditcards.ccx.pages.header.HeaderPageObjects;
import com.creditcards.pages.PageObject;
import org.jbehave.web.selenium.PerScenarioWebDriverSteps;
import org.jbehave.web.selenium.WebDriverProvider;
import org.springframework.beans.factory.annotation.Autowired;

import static org.testng.Assert.assertTrue;

public class LifecycleSteps extends PerScenarioWebDriverSteps{
    @Autowired
    private PageManager pageManager;

    public LifecycleSteps(WebDriverProvider webDriverProvider){
        super(webDriverProvider);
    }

    @Override
    public void afterScenario() throws Exception{
        HeaderPageObject headerPageObject = (HeaderPageObject)pageManager.getHeaderPageObject(HeaderPageObjects.HEADER);

        headerPageObject.clickLogout();
        headerPageObject.waitToClose();

        PageObject loginPage = pageManager.getApplicationPageObject(ApplicationPages.LOGIN);
        assertTrue(loginPage.isDisplayed(),"The Login Screen is not displayed; Logging out failed.");
        super.afterScenario();
    }
}