package com.creditcards.ccx.steps.reporting;

import com.creditcards.ccx.model.AccountSiteMetric;
import com.creditcards.ccx.pages.reporting.PerformanceReports;
import com.creditcards.ccx.pages.reporting.TopSitesByAccountPageObject;
import com.creditcards.steps.Steps;
import com.creditcards.util.Visibility;
import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.jbehave.core.model.ExamplesTable;
import org.jbehave.web.selenium.WebDriverProvider;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.Map;
import java.util.Vector;

import static org.testng.Assert.assertTrue;

@Steps
public class TopSiteByAccountReportSteps extends PerformanceReportSteps{
    TopSitesByAccountPageObject reportPageObject;

    @Autowired
    public TopSiteByAccountReportSteps(WebDriverProvider webDriverProvider){
        super(webDriverProvider);
    }

    @Override
    public void setWebPageObject(){
        reportPageObject = (TopSitesByAccountPageObject)getPageObject();
    }

    @Given("the report Top Sites by Accounts is %visibleState")
    @When("the report Top Sites by Accounts is %visibleState")
    @Then("the report Top Sites by Accounts is %visibleState")
    public void visibleState(Visibility visibleState){
        super.visibleState(PerformanceReports.TOP_SITES,visibleState);
        setWebPageObject();
    }

    @Then("the report Top Sites by Accounts title displays the %current_month")
    public void verifyChartTitle(DateTime.Property current_month){
        String title = PerformanceReports.TOP_SITES.buildHeaderTitle(current_month);
        assertTrue(reportPageObject.titleEquals(title));
    }

    @Then("the report Top Sites by Accounts includes the sites: %siteList")
    public void verifyChartDataDetails(ExamplesTable siteList){
        List<AccountSiteMetric> expectedList = buildSiteList(siteList);
        assertTrue(reportPageObject.assertAccountDetailsMatches(expectedList));
    }

    private List<AccountSiteMetric> buildSiteList(ExamplesTable table){
        List<AccountSiteMetric> accountSiteList = new Vector<>();
        List<Map<String, String>> rows = table.getRows();
        for( Map<String, String> listDetails : rows ){
            AccountSiteMetric siteMetric = new AccountSiteMetric();
            siteMetric.setUrl(listDetails.get("website_url"));
            siteMetric.setCount(listDetails.get("count"));
            siteMetric.setPercentage(listDetails.get("percent"));
            accountSiteList.add(siteMetric);
        }
        return accountSiteList;
    }
}