package com.creditcards.ccx.steps.subHeader;

import com.creditcards.ccx.pages.PageManager;
import com.creditcards.ccx.pages.converters.SubHeaderPageObjectConverter;
import com.creditcards.ccx.pages.subHeader.SubHeaderPageObjects;
import com.creditcards.pages.PageObject;
import com.creditcards.steps.AbstractSteps;
import com.creditcards.steps.Steps;
import com.creditcards.util.Visibility;
import org.jbehave.core.annotations.AsParameterConverter;
import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.jbehave.web.selenium.WebDriverProvider;
import org.springframework.beans.factory.annotation.Autowired;

import static org.testng.Assert.assertTrue;

@Steps
public abstract class SubHeaderPageObjectSteps extends AbstractSteps{
    private PageObject pageObject;

    @Autowired
    private PageManager pageManager;

    @Autowired
    public SubHeaderPageObjectSteps(WebDriverProvider webDriverProvider){
        super(webDriverProvider);
    }

    @Override
    public PageObject getPageObject(){
        return pageObject;
    }

    @Given("the %subHeaderPageObjects options are %visibleState")
    @When("the %subHeaderPageObjects options are %visibleState")
    @Then("the %subHeaderPageObjects options are %visibleState")
    public void visibleState(SubHeaderPageObjects subHeaderPageObjects, Visibility visibleState){
        pageObject = pageManager.getSubHeaderPageObject(subHeaderPageObjects);
        switch( visibleState ){
            case DISPLAYED:
                pageObject.waitUntilLoaded();
                break;
            case HIDDEN:
                pageObject.waitUntilUnLoaded();
                break;
        }
        assertTrue(pageObject.visibleStateMatches(visibleState));
    }

    @AsParameterConverter
    public SubHeaderPageObjects subHeaderPageObjects(String value){
        return (SubHeaderPageObjects)new SubHeaderPageObjectConverter().convertValue(value,String.class);
    }
}