package com.creditcards.ccx.steps.application;

import com.creditcards.ccx.utils.Issuers;
import com.creditcards.ccx.utils.Roles;
import com.creditcards.pages.PageObject;
import com.creditcards.steps.AbstractSteps;
import com.creditcards.steps.Steps;
import org.jbehave.core.annotations.Alias;
import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Named;
import org.jbehave.web.selenium.WebDriverProvider;
import org.springframework.beans.factory.annotation.Autowired;

@Steps
public class UserSteps extends AbstractSteps{
    private Roles userRole;
    private Issuers userIssuer;

    @Autowired
    public UserSteps(WebDriverProvider driverProvider){
        super(driverProvider);
    }

    @Alias("I am assigned to the Role <role>")
    @Given("I am a User assigned the Role <role>")
    public void defineUser(@Named("role") String role){
        userRole = Roles.valueOfByName(role);
    }

    @Alias("I am assigned to the Issuer <issuer>")
    @Given("I am User assigned to the Issuer <issuer>")
    public void defineIssuer(@Named("issuer") String issuer){
        userIssuer = Issuers.valueOfByName(issuer);
    }

    @Override
    public PageObject getPageObject(){
        return null;
    }

    @Override
    public void setWebPageObject(){

    }
}
