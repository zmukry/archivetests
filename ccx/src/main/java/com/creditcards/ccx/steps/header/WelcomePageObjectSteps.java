package com.creditcards.ccx.steps.header;

import com.creditcards.ccx.pages.header.WelcomePageObject;
import com.creditcards.steps.Steps;
import org.jbehave.core.annotations.Named;
import org.jbehave.core.annotations.Then;
import org.jbehave.web.selenium.WebDriverProvider;
import org.springframework.beans.factory.annotation.Autowired;

@Steps
public class WelcomePageObjectSteps extends HeaderPageObjectSteps{

    private WelcomePageObject welcomePageObject;

    @Autowired
    public WelcomePageObjectSteps(WebDriverProvider webDriverProvider){
        super(webDriverProvider);
    }

    @Then("the Welcome Message contains the users %name and %role")
    public void verifyWelcomeMessage(@Named("name") String name, @Named("role") String role){

    }

    @Then("the Logout link is displayed")
    public void logoutLinkDispalyed(){

    }

    @Override
    public void setWebPageObject(){
        welcomePageObject = (WelcomePageObject)getPageObject();
    }
}
