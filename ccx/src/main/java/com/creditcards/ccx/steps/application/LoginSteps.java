package com.creditcards.ccx.steps.application;

import com.creditcards.cache.service.CacheService;
import com.creditcards.ccx.pages.application.ApplicationPages;
import com.creditcards.ccx.pages.application.LoginPage;
import com.creditcards.ccx.pages.header.HeaderPageObject;
import com.creditcards.ccx.pages.header.HeaderPageObjects;
import com.creditcards.ccx.utils.Users;
import com.creditcards.steps.Steps;
import org.jbehave.core.annotations.AfterScenario;
import org.jbehave.core.annotations.AfterScenario.Outcome;
import org.jbehave.core.annotations.Composite;
import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Named;
import org.jbehave.core.annotations.When;
import org.jbehave.web.selenium.WebDriverProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import static org.testng.Assert.assertTrue;

@Steps
public class LoginSteps extends PageSteps{
    private Users user;
    private LoginPage loginPage;

    @Autowired
    private CacheService cacheService;

    @Value("#{settings['applicationUrl']}")
    private String applicationUrl;

    @Autowired
    public LoginSteps(WebDriverProvider webDriverProvider){
        super(webDriverProvider);
    }

    @AfterScenario(uponOutcome = Outcome.SUCCESS)
    public void logout(){
        HeaderPageObject headerPageObject = (HeaderPageObject)getPageManager().getHeaderPageObject(HeaderPageObjects.HEADER);
        headerPageObject.initElements();
        if(headerPageObject.isDisplayed()){
            headerPageObject.clickLogout();
            headerPageObject.waitToClose();
            loginPage.initElements();
            assertTrue(loginPage.isDisplayed(),"The Login Screen is not displayed; Logging out failed.");
        }
    }

    @Given("I login as <username>")
    @When("I login as <username>")
    @Composite(
            steps = { "When I open CCX Portal",
                    "Then the Login pages is displayed",
                    "When I enter my username <username>",
                    "When I enter my password",
                    "When I click the Login button",
                    "Then the Overview pages is displayed" })
    public void completeLoginProcess(@Named("username") String username){
    }

    @When("I open CCX Portal")
    public void openApplication(){
        getApplicationWebPageObject(ApplicationPages.LOGIN);
        loginPage.go(applicationUrl);
        loginPage.waitUntilLoaded();
    }

    @When("I enter my username <username>")
    public void enterUserName(@Named("username") String username){
        user = Users.valueOfByName(username);
        loginPage.enterUsername(username);
    }

    @When("I enter my password <password>")
    public void enterPassword(@Named("password") String password){
        loginPage.enterPassword(password);
    }

    @When("I enter my password")
    public void enterPassword(){
        loginPage.enterPassword(user.getPassword());
    }

    @When("I click the Login button")
    public void clickLogin(){
        loginPage.clickLogin();
    }

    @Override
    public void setWebPageObject(){
        loginPage = (LoginPage)getPageObject();
    }
}