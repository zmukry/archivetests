package com.creditcards.ccx.steps.subHeader;

import com.creditcards.ccx.pages.subHeader.CardNavigationPageObject;
import com.creditcards.ccx.pages.subHeader.SubHeaderPageObjects;
import com.creditcards.ccx.utils.Issuers;
import com.creditcards.steps.Steps;
import com.creditcards.util.Visibility;
import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.jbehave.web.selenium.WebDriverProvider;
import org.springframework.beans.factory.annotation.Autowired;

@Steps
public class CardNavigationSteps extends SubHeaderPageObjectSteps{
    CardNavigationPageObject cardNavigationPageObject;

    @Autowired
    public CardNavigationSteps(WebDriverProvider webDriverProvider){
        super(webDriverProvider);
    }

    @Override
    public void setWebPageObject(){
        cardNavigationPageObject = (CardNavigationPageObject)getPageObject();
    }

    @Given("the Card Navigation options are %visibleState")
    @When("the Card Navigation options are %visibleState")
    @Then("the Card Navigation options are %visibleState")
    public void visibleState(Visibility visibleState){
        super.visibleState(SubHeaderPageObjects.CARD_NAVIGATION,visibleState);
        setWebPageObject();
    }

    @When("the Emulate Issuer selection is %visibleState")
    public void initializeCardNavigationPage(Visibility visibleState){
        cardNavigationPageObject.isEmulateIssuerSelectionDisplayed();
    }

    @When("I emulate the issuer %issuer")
    @Then("I emulate the issuer %issuer")
    public void emulateIssuer(String issuer){
        Issuers eIssuer = Issuers.valueOfByName(issuer);
        cardNavigationPageObject.selectIssuer(eIssuer);
    }
}
