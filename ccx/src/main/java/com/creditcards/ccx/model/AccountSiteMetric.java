package com.creditcards.ccx.model;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.math.NumberUtils;

public class AccountSiteMetric{
    private String url;
    private int percentage;
    private int count;

    public String getUrl(){
        return url;
    }

    public void setUrl(String url){
        this.url = url;
    }

    public int getPercentage(){
        return percentage;
    }

    public void setPercentage(String percentage){
        this.percentage = NumberUtils.toInt(percentage);
    }

    public int getCount(){
        return count;
    }

    public void setCount(String count){
        this.count = NumberUtils.toInt(count);
    }

    @Override
    public int hashCode(){
        return new HashCodeBuilder().append(this.url).append(this.percentage).append(this.count).toHashCode();
    }

    @Override
    public boolean equals(Object obj){
        if( obj == null ){
            return false;
        }
        if( getClass() != obj.getClass() ){
            return false;
        }
        final AccountSiteMetric other = (AccountSiteMetric)obj;
        return new EqualsBuilder().append(this.url,other.url).append(this.percentage,other.percentage)
                                  .append(this.count,other.count).isEquals();
    }
}
