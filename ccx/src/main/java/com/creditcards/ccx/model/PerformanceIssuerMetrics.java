package com.creditcards.ccx.model;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.commons.lang3.text.StrBuilder;

public class PerformanceIssuerMetrics{
    private int newWebsites;
    private int activeWebsites;
    private int totalClicks;
    private int totalAccounts;

    public int getNewWebsites(){
        return newWebsites;
    }

    public void setNewWebsites(String newWebsites){
        this.newWebsites = NumberUtils.toInt(newWebsites);
    }

    public int getActiveWebsites(){
        return activeWebsites;
    }

    public void setActiveWebsites(String activeWebsites){
        this.activeWebsites = NumberUtils.toInt(activeWebsites);
    }

    public int getTotalClicks(){
        return totalClicks;
    }

    public void setTotalClicks(String totalClicks){
        this.totalClicks = NumberUtils.toInt(totalClicks);
    }

    public int getTotalAccounts(){
        return totalAccounts;
    }

    public void setTotalAccounts(String totalAccounts){
        this.totalAccounts = NumberUtils.toInt(totalAccounts);
    }

    @Override
    public int hashCode(){
        return new HashCodeBuilder().append(this.newWebsites).append(this.activeWebsites).append(this.totalClicks)
                                    .append(this.totalAccounts).toHashCode();
    }

    @Override
    public boolean equals(Object obj){
        if( obj == null ){
            return false;
        }
        if( getClass() != obj.getClass() ){
            return false;
        }
        final PerformanceIssuerMetrics other = (PerformanceIssuerMetrics)obj;
        return new EqualsBuilder().append(this.newWebsites,other.newWebsites)
                                  .append(this.activeWebsites,other.activeWebsites)
                                  .append(this.totalClicks,other.totalClicks)
                                  .append(this.totalAccounts,other.totalAccounts).isEquals();
    }

    @Override
    public String toString(){
        StrBuilder builder = new StrBuilder();
        builder.append("Active Websites:" + activeWebsites).append(Character.LINE_SEPARATOR)
               .append("New Websites:" + newWebsites).append(Character.LINE_SEPARATOR)
               .append("Total Clicks:" + totalClicks).append(Character.LINE_SEPARATOR)
               .append("Total Accounts" + totalAccounts).append(Character.LINE_SEPARATOR);
        return builder.toString();
    }
}
