package com.creditcards.ccx.pages.reporting;

import com.creditcards.ccx.model.AccountSiteMetric;
import com.creditcards.pages.AbstractPageObject;
import org.apache.commons.collections.ListUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.text.StrBuilder;
import org.jbehave.web.selenium.WebDriverProvider;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;

import java.util.List;
import java.util.Vector;

public class TopSitesByAccountPageObject extends AbstractPageObject{

    @FindBys({ @FindBy(id = "allReportsContainer"),@FindBy(css = "div.span8 div.box:nth-child(2)") })
    private WebElement reportContainer;

    @FindBy(id = "topsitesHeaderTitle")
    private WebElement overviewHeader;

    @FindBy(id = "dropdown-topsites-title")
    private WebElement overviewHeaderTitle;

    @FindBy(id = "topSitesDatePicker")
    private WebElement reportPicker;

    @FindBy(id = "topSitesReport")
    private WebElement reportContent;

    @FindBys({ @FindBy(id = "topSitesReport"),@FindBy(className = "barhldr") })
    private List<WebElement> topSites;

    public TopSitesByAccountPageObject(WebDriverProvider webDriverProvider){
        super(webDriverProvider);
    }

    public enum ChartElement{
        TITLE("newWebsitesContainer"),
        LEGEND("legend"),
        CONTEXT_MENU("contextMenu");

        private final String id;

        ChartElement(String id){
            this.id = id;
        }

        String getId(){
            return id;
        }
    }

    @Override
    public By displayLocator(){
        return By.cssSelector("div#allReportsContainer div.span8 div.box:nth-child(2)");
    }

    @Override
    public WebElement displayElement(){
        return reportContainer;
    }

    public boolean titleEquals(String expected){
        return StringUtils.equalsIgnoreCase(overviewHeaderTitle.getText(),expected);
    }

    public boolean accountCountMatch(int expected){
        return topSites.size() == expected;
    }

    public boolean assertAccountDetailsMatches(List<AccountSiteMetric> expectedList){
        return ListUtils.intersection(expectedList,parseSiteList()).size() == expectedList.size();
    }

    private List<AccountSiteMetric> parseSiteList(){
        List<AccountSiteMetric> accountSiteList = new Vector<>();
        StrBuilder parser = new StrBuilder();
        for( WebElement siteElement : topSites ){
            AccountSiteMetric siteMetric = new AccountSiteMetric();
            parser.append(siteElement.findElement(By.className("progresstitle")).getText());
            parser.deleteFirst("1.").trim();
            siteMetric.setUrl(parser.toString());

            parser.clear();
            parser.append(siteElement.findElement(By.className("progress_sales")).getText());
            parser.deleteFirst("Accounts").trim();

            siteMetric.setCount(parser.toString());

            parser.clear();
            parser.append(siteElement.findElement(By.className("bar")).getAttribute("style"));
            parser.deleteFirst("width:");
            parser.deleteFirst("%;");
            parser.trim();

            siteMetric.setPercentage(parser.toString());

            accountSiteList.add(siteMetric);
        }
        return accountSiteList;
    }
}
