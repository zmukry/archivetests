package com.creditcards.ccx.pages.subHeader;

import com.creditcards.pages.AbstractPageObject;
import org.jbehave.web.selenium.WebDriverProvider;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class SubHeaderPageObject extends AbstractPageObject{

    @FindBy(id = "subhead")
    private WebElement subHeaderContainer;

    public SubHeaderPageObject(WebDriverProvider webDriverProvider){
        super(webDriverProvider);
    }

    @Override
    public By displayLocator(){
        return By.id("subhead");
    }

    @Override
    public WebElement displayElement(){
        return subHeaderContainer;
    }
}