package com.creditcards.ccx.pages.header;

import com.creditcards.pages.AbstractPageObject;
import org.jbehave.web.selenium.WebDriverProvider;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;

public class MainNavigationTabPageObject extends AbstractPageObject{

    @FindBy(id = "nav-main")
    private WebElement navigationMenu;

    @FindBys({ @FindBy(id = "nav-main"),@FindBy(tagName = "li"),@FindBy(className = "selected") })
    private WebElement selectedTab;

    @FindBy(css = "#nav_aff_reporting > a")
    private WebElement reportDashboardLink;

    public MainNavigationTabPageObject(WebDriverProvider webDriverProvider){
        super(webDriverProvider);
    }

    @Override
    public By displayLocator(){
        return By.id("nav-main");
    }

    @Override
    public WebElement displayElement(){
        return navigationMenu;
    }

    public WebElement navigationMenu(){
        return navigationMenu;
    }

    public void openReportDashboard(){
        reportDashboardLink.click();
    }
}
