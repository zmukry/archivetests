package com.creditcards.ccx.pages;

import com.creditcards.cache.service.CacheService;
import com.creditcards.ccx.pages.application.ApplicationPages;
import com.creditcards.ccx.pages.application.LoadingDialogPage;
import com.creditcards.ccx.pages.application.LoginPage;
import com.creditcards.ccx.pages.application.OverviewPage;
import com.creditcards.ccx.pages.application.ReportDashboardPage;
import com.creditcards.ccx.pages.content.CardIntroductionPageObject;
import com.creditcards.ccx.pages.content.ContentPageObjects;
import com.creditcards.ccx.pages.content.ProductListPageObject;
import com.creditcards.ccx.pages.header.HeaderPageObject;
import com.creditcards.ccx.pages.header.HeaderPageObjects;
import com.creditcards.ccx.pages.header.MainNavigationTabPageObject;
import com.creditcards.ccx.pages.reporting.DashboardReportPageObject;
import com.creditcards.ccx.pages.reporting.PerformanceBarPageObject;
import com.creditcards.ccx.pages.reporting.PerformanceReports;
import com.creditcards.ccx.pages.reporting.TopCardsByAccountPageObject;
import com.creditcards.ccx.pages.reporting.TopSitesByAccountPageObject;
import com.creditcards.ccx.pages.subHeader.CardNavigationPageObject;
import com.creditcards.ccx.pages.subHeader.SubHeaderPageObject;
import com.creditcards.ccx.pages.subHeader.SubHeaderPageObjects;
import com.creditcards.pages.PageObject;
import org.jbehave.web.selenium.WebDriverProvider;
import org.springframework.beans.factory.annotation.Autowired;

public final class PageManager{

    @Autowired
    private CacheService cacheService;

    private final WebDriverProvider webDriverProvider;

    public PageManager(WebDriverProvider driverProvider){
        this.webDriverProvider = driverProvider;
    }

    public PageObject getPerformanceReportPageObject(PerformanceReports performanceReport){
        if( !cacheService.containsKey(performanceReport) ){
            cacheService.put(performanceReport,newPerformanceReportPageObject(performanceReport));
        }
        return (PageObject)cacheService.get(performanceReport);
    }

    public PageObject getApplicationPageObject(ApplicationPages page){
        if( !cacheService.containsKey(page) ){
            cacheService.put(page,newApplicationPageObject(page));
        }
        return (PageObject)cacheService.get(page);
    }

    public PageObject getHeaderPageObject(HeaderPageObjects page){
        if( !cacheService.containsKey(page) ){
            cacheService.put(page,newHeaderPageObject(page));
        }
        return (PageObject)cacheService.get(page);
    }

    public PageObject getSubHeaderPageObject(SubHeaderPageObjects page){
        if( !cacheService.containsKey(page) ){
            cacheService.put(page,newSubHeaderPageObject(page));
        }
        return (PageObject)cacheService.get(page);
    }

    public PageObject getContentPageObject(ContentPageObjects page){
        if( !cacheService.containsKey(page) ){
            cacheService.put(page,newContentPageObject(page));
        }
        return (PageObject)cacheService.get(page);
    }

    private PageObject newPerformanceReportPageObject(PerformanceReports reportPage){
        PageObject page = null;
        switch( reportPage ){
            case DASHBOARD_REPORT:
                page = new DashboardReportPageObject(webDriverProvider);
                break;
            case TOP_CARDS:
                page = new TopCardsByAccountPageObject(webDriverProvider);
                break;
            case TOP_SITES:
                page = new TopSitesByAccountPageObject(webDriverProvider);
                break;
            case PERFORMANCE_BAR:
                page = new PerformanceBarPageObject(webDriverProvider);
                break;
        }
        return page;
    }

    private PageObject newApplicationPageObject(ApplicationPages pageType){
        PageObject page = null;
        switch( pageType ){
            case LOADING:
                page = new LoadingDialogPage(webDriverProvider);
                break;
            case LOGIN:
                page = new LoginPage(webDriverProvider);
                break;
            case OVERVIEW:
                page = new OverviewPage(webDriverProvider);
                break;
            case REPORT_DASHBOARD:
                page = new ReportDashboardPage(webDriverProvider);
                break;
        }
        return page;
    }

    private PageObject newContentPageObject(ContentPageObjects contentPageObject){
        PageObject page = null;
        switch( contentPageObject ){
            case PRODUCT_LIST:
                page = new ProductListPageObject(webDriverProvider);
                break;
            case CARD_INTRODUCTION:
                page = new CardIntroductionPageObject(webDriverProvider);
                break;
        }
        return page;
    }

    private PageObject newHeaderPageObject(HeaderPageObjects headerPage){
        PageObject page = null;
        switch( headerPage ){
            case MAIN_NAVIGATION:
                page = new MainNavigationTabPageObject(webDriverProvider);
                break;
            case HEADER:
                page = new HeaderPageObject(webDriverProvider);
                break;
        }
        page.waitUntilLoaded();
        return page;
    }

    private PageObject newSubHeaderPageObject(SubHeaderPageObjects headerPage){
        PageObject page = null;
        switch( headerPage ){
            case CARD_NAVIGATION:
                page = new CardNavigationPageObject(webDriverProvider);
                break;
            case SUB_HEADER:
                page = new SubHeaderPageObject(webDriverProvider);
                break;
        }
        return page;
    }
}