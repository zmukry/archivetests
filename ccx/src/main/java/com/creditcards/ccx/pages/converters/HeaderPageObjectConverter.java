package com.creditcards.ccx.pages.converters;

import com.creditcards.ccx.pages.header.HeaderPageObjects;
import org.jbehave.core.steps.ParameterConverters;
import org.jbehave.core.steps.ParameterConverters.EnumConverter;

import java.lang.reflect.Type;

public class HeaderPageObjectConverter extends EnumConverter implements ParameterConverters.ParameterConverter{
    @Override
    public boolean accept(Type type){
        return type instanceof Class<?> && HeaderPageObjects.class.isAssignableFrom((Class<?>)type);
    }

    @Override
    public Object convertValue(String name, Type type){
        return HeaderPageObjects.valueOfByName(name);
    }
}
