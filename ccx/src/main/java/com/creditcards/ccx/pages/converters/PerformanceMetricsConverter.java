package com.creditcards.ccx.pages.converters;

import com.creditcards.ccx.pages.reporting.PerformanceBarPageObject.PerformanceMetrics;
import org.jbehave.core.steps.ParameterConverters;
import org.jbehave.core.steps.ParameterConverters.EnumConverter;

import java.lang.reflect.Type;

public class PerformanceMetricsConverter extends EnumConverter implements ParameterConverters.ParameterConverter{
    @Override
    public boolean accept(Type type){
        return type instanceof Class<?> && PerformanceMetrics.class.isAssignableFrom((Class<?>)type);
    }

    @Override
    public Object convertValue(String name, Type type){
        return PerformanceMetrics.valueOfByDisplay(name);
    }
}

