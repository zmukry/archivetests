package com.creditcards.ccx.pages.converters;

import com.creditcards.ccx.pages.subHeader.SubHeaderPageObjects;
import org.jbehave.core.steps.ParameterConverters;
import org.jbehave.core.steps.ParameterConverters.EnumConverter;

import java.lang.reflect.Type;

public class SubHeaderPageObjectConverter extends EnumConverter implements ParameterConverters.ParameterConverter{
    @Override
    public boolean accept(Type type){
        return type instanceof Class<?> && SubHeaderPageObjects.class.isAssignableFrom((Class<?>)type);
    }

    @Override
    public Object convertValue(String name, Type type){
        return SubHeaderPageObjects.valueOfByName(name);
    }
}
