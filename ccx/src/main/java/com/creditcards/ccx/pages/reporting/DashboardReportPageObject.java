package com.creditcards.ccx.pages.reporting;

import com.creditcards.pages.AbstractPageObject;
import com.creditcards.util.Visibility;
import org.apache.commons.collections.ListUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.jbehave.web.selenium.WebDriverProvider;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;

import java.text.DateFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class DashboardReportPageObject extends AbstractPageObject{

    @FindBys({ @FindBy(id = "allReportsContainer"),@FindBy(css = "div.span8 div.box:nth-child(1)") })
    private WebElement reportContainer;

    @FindBy(id = "overviewHeaderTitle")
    private WebElement overviewHeader;

    @FindBy(id = "dropdown-report-title")
    private WebElement overviewHeaderTitle;

    @FindBys({ @FindBy(id = "overviewHeaderTitle"),@FindBy(css = "a[data-toggle='dropdown']") })
    private WebElement reportDropdownSelect;

    @FindBy(id = "filter-btn")
    private WebElement filterOption;

    @FindBys({ @FindBy(id = "overviewHeaderTitle"),@FindBy(id = "reportsDropdown") })
    private WebElement reportsDropdown;

    @FindBy(id = "charthldr")
    private WebElement primaryChart;

    @FindBys({ @FindBy(id = "exportChartDataButton"),@FindBy(tagName = "a") })
    private WebElement exportChartDataButton;

    @FindBys({ @FindBy(id = "exportChartDataText"),@FindBy(tagName = "a") })
    private WebElement exportChartDataLink;

    @FindBys({ @FindBy(id = "charthldr"),@FindBy(className = "highcharts-input-group") })
    private WebElement chartDateRange;

    @FindBys({ @FindBy(id = "charthldr"),@FindBy(className = "highcharts-button") })
    private WebElement chartContextMenu;

    @FindBys({ @FindBy(id = "charthldr"),@FindBy(css = "div.highcharts-axis span") })
    private WebElement yAxisLabel;

    public enum ChartElement{
        TITLE("dropdown-report-title","title"),
        FILTER_BUTTON("filter-btn","Filter Options button"),
        EXPORT_DATA_LINK("exportChartDataText","Export Chart Data link"),
        EXPORT_DATA_ICON("exportChartDataButton","Export Chart Data icon"),
        CONTEXT_MENU("","context menu"),
        YAXIS_LABEL("highcharts-axis","y-axis label");

        private final String id;
        private final String name;

        private ChartElement(String id, String name){
            this.id = id;
            this.name = name;
        }

        public String getName(){
            return name;
        }

        public static ChartElement valueOfByName(String name){
            ChartElement chartElement = null;
            for( ChartElement element : ChartElement.values() ){
                if( StringUtils.equalsIgnoreCase(name,element.name) ){
                    chartElement = element;
                    break;
                }
            }
            return chartElement;
        }
    }

    public enum DashboardReports{
        CLICKS_BY_DAY("getClicksByDay","Clicks By Day"),
        ACCOUNTS_BY_CLICK_DATE("getAccountsByClickDate","Accounts By Click Date"),
        ACCOUNTS_BY_PROCESS_DATE("getAccountsByProcessDate","Accounts By Process Date"),
        APPLICATION_RATE("getApplicationRate","Application Rate"),
        APPROVAL_RATE("getApprovalRate","Approval Rate"),
        CONVERSION_RATE("getConversionRate","Conversion Rate"),
        CLICK_SHARE("getClickShare","Click Share %"),
        ACCOUNT_SHARE("getAccountShare","Account Share %");

        private final String id;
        private final String name;

        private DashboardReports(String id, String name){
            this.id = id;
            this.name = name;
        }

        public static DashboardReports valueOfByName(String name){
            DashboardReports report = null;
            for( DashboardReports dashboardReport : DashboardReports.values() ){
                if( StringUtils.equalsIgnoreCase(name,dashboardReport.name) ){
                    report = dashboardReport;
                    break;
                }
            }
            return report;
        }

        public String getId(){
            return id;
        }

        public String getName(){
            return name;
        }
    }

    public DashboardReportPageObject(WebDriverProvider webDriverProvider){
        super(webDriverProvider);
    }

    @Override
    public By displayLocator(){
        return By.cssSelector("#allReportsContainer div.span8");
    }

    @Override
    public WebElement displayElement(){
        return reportContainer;
    }

    public boolean reportIsSelected(DashboardReports report){
        return true;
    }

    public boolean chartElementVisibleStateMatches(ChartElement chartElement, Visibility visibility){
        boolean stateMatches = false;
        switch( chartElement ){
            case CONTEXT_MENU:
                stateMatches = elementIsDisplayed(chartContextMenu);
                break;
            case EXPORT_DATA_ICON:
                stateMatches = elementIsDisplayed(exportChartDataButton);
                break;
            case EXPORT_DATA_LINK:
                stateMatches = elementIsDisplayed(exportChartDataLink);
                break;
            case FILTER_BUTTON:
                stateMatches = elementIsDisplayed(filterOption);
                break;
            case YAXIS_LABEL:
                stateMatches = elementIsDisplayed(yAxisLabel);
                break;
        }
        return stateMatches;
    }

    public boolean chartElementLabelMatches(ChartElement chartElement, String value){
        return true;
    }

    public boolean yAxisTextMatches(String expected){
        return StringUtils.equalsIgnoreCase(expected,yAxisLabel.getText());
    }

    public boolean dateRangeMatches(Date expectedFrom, Date expectedTo) throws ParseException{
        WebElement fromInput = chartDateRange
                .findElement(By.cssSelector("g.highcharts-input-group g:nth-child(2) tspan"));
        DateFormat dateFormat = DateFormat.getDateInstance(DateFormat.MEDIUM);

        Date actualFrom = dateFormat.parse(fromInput.getText());

        WebElement toInput = chartDateRange
                .findElement(By.cssSelector("g.highcharts-input-group g:nth-child(4) tspan"));
        Date actualTo = dateFormat.parse(toInput.getText());

        return DateUtils.isSameDay(expectedFrom,actualFrom) && DateUtils.isSameDay(expectedTo,actualTo);
    }

    public boolean reportSelectionsEquals(List<String> expectedReportList){
        Actions builder = new Actions(getDriverProvider().get());
        Action reportClick = builder.moveToElement(reportDropdownSelect).click(reportDropdownSelect).build();
        reportClick.perform();

        List<WebElement> reportLinks = reportsDropdown.findElements(By.tagName("a"));
        List<String> actualReportList = new ArrayList<>();
        for( WebElement report : reportLinks ){
            actualReportList.add(report.getText());
        }

        return ListUtils.intersection(expectedReportList,actualReportList).size() == expectedReportList.size();
    }
}