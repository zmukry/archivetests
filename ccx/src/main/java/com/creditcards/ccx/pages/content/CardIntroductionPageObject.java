package com.creditcards.ccx.pages.content;

import com.creditcards.pages.AbstractPageObject;
import org.apache.commons.lang3.StringUtils;
import org.jbehave.web.selenium.WebDriverProvider;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CardIntroductionPageObject extends AbstractPageObject{
    private static final Logger logger = LoggerFactory.getLogger(CardIntroductionPageObject.class);

    @FindBy(id = "card_intro")
    private WebElement introContentElement;

    public CardIntroductionPageObject(WebDriverProvider webDriverProvider){
        super(webDriverProvider);
    }

    @Override
    public By displayLocator(){
        return By.id("card_intro");
    }

    @Override
    public WebElement displayElement(){
        return introContentElement;
    }

    public boolean textContentMatches(String expecetdContent){
        return StringUtils.equalsIgnoreCase(introContentElement.getText(),expecetdContent);
    }
}
