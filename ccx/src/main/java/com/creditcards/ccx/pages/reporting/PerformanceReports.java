package com.creditcards.ccx.pages.reporting;

import com.creditcards.lang.StringCharacter;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.text.StrBuilder;
import org.joda.time.DateTime;

public enum PerformanceReports{
    DASHBOARD_REPORT("overviewHeaderTitle","charthldr","Dashboard Report"),
    PERFORMANCE_BAR("performanceHeaderTitle","performanceTotalsBar","Performance Bar"),
    TOP_CARDS("topcardsHeaderTitle","piechart","Top Cards by Accounts"),
    TOP_SITES("topcardsHeaderTitle","topSitesReport","Top Sites by Accounts");

    private final String titleID;
    private final String contentID;
    private final String name;

    private PerformanceReports(String titleID, String contentID, String name){
        this.titleID = titleID;
        this.contentID = contentID;
        this.name = name;
    }

    public String getName(){
        return name;
    }

    public String getTitleID(){
        return titleID;
    }

    public String getContentID(){
        return contentID;
    }

    public static PerformanceReports valueOfByName(String name){
        PerformanceReports reportPage = null;
        for( PerformanceReports page : PerformanceReports.values() ){
            if( StringUtils.equalsIgnoreCase(name,page.name) ){
                reportPage = page;
                break;
            }
        }
        return reportPage;
    }

    public String buildHeaderTitle(DateTime.Property currentMonth){
        DateTime.Property currentYear = new DateTime().year();
        StrBuilder builder = new StrBuilder();

        builder.append(name).append(StringCharacter.SPACE).append("for").append(StringCharacter.SPACE)
               .append(currentMonth.getAsShortText()).append(StringCharacter.SPACE).append(currentYear.getAsText());

        return builder.toString();
    }
}
