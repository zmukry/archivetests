package com.creditcards.ccx.pages.application;

import com.creditcards.pages.AbstractPageObject;
import org.apache.commons.lang3.text.StrBuilder;
import org.jbehave.web.selenium.WebDriverProvider;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;

public class LoginPage extends AbstractPageObject{

    private enum PageElements{
        TITLE("CardSynergy Manager"),
        PARENT("login-wrapper"),
        FORM_ID("login"),
        USERNAME("username"),
        PASSWORD("password"),
        URL("login.php");

        private final String value;

        PageElements(String element){
            this.value = element;
        }

        public By byIDSelector(){
            return By.id(value);
        }

        public String toString(){
            return value;
        }
    }

    @FindBy(id = "login-wrapper")
    @CacheLookup
    private WebElement parentContainer;

    @FindBy(id = "username")
    @CacheLookup
    private WebElement loginUsername;

    @FindBy(id = "password")
    @CacheLookup
    private WebElement loginPassword;

    @FindBy(id = "submit_login")
    private WebElement loginButton;

    public LoginPage(WebDriverProvider webDriverProvider){
        super(webDriverProvider);
    }

    public void go(String applicationUrl){
        StrBuilder builder = new StrBuilder();
        builder.append("https://").append(applicationUrl).append("/").append(PageElements.URL.toString());
        get(builder.toString());
    }

    public void enterUsername(String email){
        loginUsername.clear();
        loginUsername.sendKeys(email);
    }

    public void enterPassword(String password){
        loginPassword.clear();
        loginPassword.sendKeys(password);
    }

    public void clickLogin(){
        loginButton.click();
    }

    @Override
    public By displayLocator(){
        return PageElements.PARENT.byIDSelector();
    }

    @Override
    public WebElement displayElement(){
        return parentContainer;
    }
}
