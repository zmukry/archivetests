package com.creditcards.ccx.pages.subHeader;

import com.creditcards.ccx.utils.Issuers;
import com.creditcards.model.OverviewSummary;
import com.creditcards.pages.AbstractContainerPageObject;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.commons.lang3.text.StrBuilder;
import org.jbehave.web.selenium.WebDriverProvider;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import static org.apache.commons.lang3.StringUtils.EMPTY;

public class CardNavigationPageObject extends AbstractContainerPageObject{

    @FindBy(id = "nav")
    private WebElement cardNavigationMenu;

    @FindBy(id = "nav-cards")
    private WebElement cardsList;

    @FindBy(id = "count-overview")
    private WebElement overviewHoverIcon;

    @FindBys({ @FindBy(id = "nav-cards"),@FindBy(id = "count-overview") })
    private WebElement countoverview;

    @FindBys({ @FindBy(id = "nav-cards"),@FindBy(id = "summary") })
    private WebElement cardListOverviewSummary;

    @FindBys({ @FindBy(id = "nav-cards"),@FindBy(id = "filterMerchant") })
    private WebElement filterMerchant;

    @FindBys({ @FindBy(id = "nav-cards"),@FindBy(id = "filterMerchant") })
    private WebElement filterMerchant2;

    @FindBys({ @FindBy(id = "nav-cards"),@FindBy(css = "li a:has(img[alt='Add New Card'])") })
    private WebElement addNewCardIcon;

    @FindBys({ @FindBy(id = "nav-cards"),@FindBy(css = "li a:has(img[alt='Hide Offline'])") })
    private WebElement hideOfflineIcon;

    @FindBys({ @FindBy(id = "nav-cards"),@FindBy(css = "li a:has(img[alt='Show Offline'])") })
    private WebElement showOfflineIcon;

    @FindBys({ @FindBy(id = "nav-cards"),@FindBy(css = "li a:has(img[alt='Overview'])") })
    private WebElement overviewIcon;

    @FindBys({ @FindBy(id = "nav-cards"),@FindBy(css = "li a:has(img[alt='Print Card List'])") })
    private WebElement printCardListIcon;

    public static enum CardNavigationMenuBar{
        ADD_CARD(EMPTY,"Add New Card","Add New Card","add-card-icon.png","/add.php"),
        HIDE_OFFLINE("offline-toggle","Hide Offline","Hide Offline","hide-offline.png",EMPTY),
        SHOW_OFFLINE("offline-toggle","Show Offline","Show Offline","show-offline.png",EMPTY),
        OVERVIEW("count-overview","Overview","Overview","overview.png",EMPTY),
        PRINT_CARD_LIST(EMPTY,"Print Card List","Print Card List","print.png",EMPTY),
        EMMULATE_ISSUER("filterMerchant","Emulate issuer",EMPTY,EMPTY,EMPTY);

        private final String id;
        private final String name;
        private final String alt;
        private final String image;
        private final String href;
        private final By idSelector;

        private CardNavigationMenuBar(String id, String name, String alt, String image, String href){
            this.id = id;
            this.name = name;
            this.href = href;
            this.idSelector = By.id(id);
            this.alt = alt;
            this.image = image;
        }

        public static CardNavigationMenuBar valueOfByName(String name){
            CardNavigationMenuBar result = null;
            for( CardNavigationMenuBar navigation : CardNavigationMenuBar.values() ){
                if( StringUtils.equalsIgnoreCase(navigation.name,name) ){
                    result = navigation;
                    break;
                }
            }
            return result;
        }

        public static CardNavigationMenuBar valueOfByHref(String href){
            CardNavigationMenuBar result = null;
            for( CardNavigationMenuBar navigation : CardNavigationMenuBar.values() ){
                if( StringUtils.endsWithIgnoreCase(navigation.href,href) ){
                    result = navigation;
                    break;
                }
            }
            return result;
        }

        public static CardNavigationMenuBar valueOfById(String id){
            CardNavigationMenuBar result = null;
            for( CardNavigationMenuBar navigation : CardNavigationMenuBar.values() ){
                if( StringUtils.endsWithIgnoreCase(navigation.id,id) ){
                    result = navigation;
                    break;
                }
            }
            return result;
        }

        public By byIdSelector(){
            return this.idSelector;
        }

        public String getHref(){
            return href;
        }

        public String getId(){
            return id;
        }

        public String getDisplayName(){
            return name;
        }

        public String getImage(){
            return image;
        }

        public String getAlt(){
            return alt;
        }
    }

    public static enum CardOverviewSummary{
        NEW("New Cards Not Yet Submitted (NEW):",1),
        SAVED("Changes Saved but Not Yet Submitted (SAVED):",2),
        ERROR("Cards Submitted with Errors (ERROR):",3),
        UP_TO_DATE("Cards with No Pending Changes or Deletion (UP TO DATE):",4),
        CHANGES_PENDING("Cards with Changes Pending (CHANGES PENDING):",5),
        DELETION_PENDING("Cards with Deletion Pending (DELETION PENDING):",6),
        TOTAL("Total number of cards:",7);

        private final String display;
        private final int index;
        private final String displaySelector;
        private final String valueSelector;

        private CardOverviewSummary(String display, int index){
            this.display = display;
            this.index = index;
            this.displaySelector = new StrBuilder().append("tr:nth-child(").append(index).append(") td:first")
                                                   .toString();
            this.valueSelector = new StrBuilder().append("tr:nth-child(").append(index).append(") td:nth-child(2)")
                                                 .toString();
        }

        public By getDisplaySelector(){
            return By.cssSelector(displaySelector);
        }

        public By getValueSelector(){
            return By.cssSelector(valueSelector);
        }
    }

    public CardNavigationPageObject(WebDriverProvider webDriverProvider){
        super(webDriverProvider);
    }

    @Override
    public void initPageElements(){
        waitElementToDisplay(cardNavigationMenu);
        waitElementToDisplay(filterMerchant);
    }

    @Override
    public By displayLocator(){
        return By.id("nav-cards");
    }

    @Override
    public WebElement displayElement(){
        return cardNavigationMenu;
    }

    public void clickAddNewCardIcon(){
        // addCardIcon.click();
    }

    public void clickHideOfflineIcon(){
        hideOfflineIcon.click();
    }

    public void clickShowOfflineIcon(){
        showOfflineIcon.click();
    }

    public void clickPrintCardListIcon(){
        printCardListIcon.click();
    }

    public void hoverOverviewIcon(){
        Actions builder = new Actions(getDriverProvider().get());
        builder.moveToElement(overviewHoverIcon);
        builder.build().perform();

        new WebDriverWait(getDriverProvider().get(),10).until(ExpectedConditions.visibilityOf(cardListOverviewSummary));
    }

    public void selectIssuer(Issuers issuer){
        Select select = new Select(filterMerchant);
        select.selectByValue(String.valueOf(issuer.getId()));
    }

    public boolean overviewSummaryEquals(OverviewSummary expected){
        OverviewSummary actual = parseCardOverviewSummary();
        return ObjectUtils.equals(expected,actual);
    }

    public boolean isEmulateIssuerSelectionDisplayed(){
        Select select = new Select(filterMerchant);
        return null != filterMerchant && elementIsDisplayed(select.getFirstSelectedOption());
    }

    private OverviewSummary parseCardOverviewSummary(){
        OverviewSummary overviewSummary = new OverviewSummary();
        for( CardOverviewSummary overviewSummaryRow : CardOverviewSummary.values() ){
            int count = NumberUtils
                    .toInt(cardListOverviewSummary.findElement(overviewSummaryRow.getValueSelector()).getText());
            switch( overviewSummaryRow ){
                case NEW:
                    overviewSummary.setNewCards(count);
                    break;
                case SAVED:
                    overviewSummary.setSavedCards(count);
                    break;
                case ERROR:
                    overviewSummary.setCardsWithErrors(count);
                    break;
                case UP_TO_DATE:
                    overviewSummary.setCardsUpToDate(count);
                    break;
                case CHANGES_PENDING:
                    overviewSummary.setChangesPending(count);
                    break;
                case DELETION_PENDING:
                    overviewSummary.setDeletionsPending(count);
                    break;
                case TOTAL:
                    overviewSummary.setTotalCards(count);
                    break;
            }
        }
        return overviewSummary;
    }
}
