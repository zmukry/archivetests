package com.creditcards.ccx.pages.reporting;

import com.creditcards.ccx.model.PerformanceIssuerMetrics;
import com.creditcards.lang.StringCharacter;
import com.creditcards.model.PerformanceMetric;
import com.creditcards.pages.AbstractPageObject;
import com.creditcards.util.Visibility;
import org.apache.commons.collections.ListUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.jbehave.web.selenium.WebDriverProvider;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;
import java.util.Vector;

public class PerformanceBarPageObject extends AbstractPageObject{

    @FindBy(id = "performanceTotalsBar")
    private WebElement performanceTotalsBar;

    @FindBy(id = "performanceHeaderTitle")
    private WebElement performanceHeaderTitle;

    @FindBy(id = "newWebsitesContainer")
    private WebElement newWebsitesContainer;

    @FindBy(id = "newWebsitesTitle")
    private WebElement newWebsitesTitle;

    @FindBy(id = "newWebsitesTotal")
    private WebElement newWebsitesTotal;

    @FindBy(id = "activeWebsitesContainer")
    private WebElement activeWebsitesContainer;

    @FindBy(id = "activeWebsitesTotal")
    private WebElement activeWebsitesTotal;

    @FindBy(id = "activeWebsitesTitle")
    private WebElement activeWebsitesTitle;

    @FindBy(id = "totalClicksContainer")
    private WebElement totalClicksContainer;

    @FindBy(id = "totalClicksTitle")
    private WebElement totalClicksTitle;

    @FindBy(id = "totalClicksTotal")
    private WebElement totalClicksTotal;

    @FindBy(id = "totalAccountsContainer")
    private WebElement totalAccountsContainer;

    @FindBy(id = "totalAccountsTotal")
    private WebElement totalAccountsTotal;

    @FindBy(id = "totalAccountsTitle")
    private WebElement totalAccountsTitle;

    private List<String> actualMetricTitles;

    private List<PerformanceMetric> actualPerformanceMetricList;

    private PerformanceIssuerMetrics actualPerformanceMetrics;

    public enum MetaPageData{
        PERFORMANCE_TOTALS_BAR("performanceTotalsBar"),
        TITLE("performanceHeaderTitle"),
        NEW_WEBSITES_CONTAINER("newWebsitesContainer"),
        NEW_WEBSITES_TITLE("newWebsitesTitle"),
        NEW_WEBSITES_TOTAL("newWebsitesTotal"),
        ACTIVE_WEBSITES_CONTAINER("activeWebsitesContainer"),
        ACTIVE_WEBSITES_TITLE("activeWebsitesTitle"),
        ACTIVE_WEBSITES_TOTAL("activeWebsitesTotal"),
        TOTAL_CLICKS_CONTAINER("totalClicksContainer"),
        TOTAL_CLICKS_TITLE("totalClicksTitle"),
        TOTAL_CLICKS_TOTAL("totalClicksTotal"),
        TOTAL_ACCOUNTS_CONTAINER("totalAccountsContainer"),
        TOTAL_ACCOUNTS_TITLE("totalAccountsTitle"),
        TOTAL_ACCOUNTS_TOTAL("totalAccountsTotal");

        private final String id;

        MetaPageData(String id){
            this.id = id;
        }

        String getId(){
            return id;
        }
    }

    public enum PerformanceMetrics{
        NEW_WEBSITES("new_websites","NEW WEBSITES"),
        ACTIVE_WEBSITES("active_websites","ACTIVE WEBSITES"),
        TOTAL_CLICKS("total_clicks","TOTAL CLICKS"),
        TOTAL_ACCOUNTS("total_accounts","TOTAL ACCOUNTS");

        private final String id;
        private final String display;

        private PerformanceMetrics(String id, String display){
            this.id = id;
            this.display = display;
        }

        public static PerformanceMetrics valueOfByDisplay(String display){
            PerformanceMetrics result = null;
            for( PerformanceMetrics component : PerformanceMetrics.values() ){
                if( org.apache.commons.lang3.StringUtils.equalsIgnoreCase(display,component.display) ){
                    result = component;
                    break;
                }
            }
            return result;
        }

        public static PerformanceMetrics valueOfById(String id){
            PerformanceMetrics result = null;
            for( PerformanceMetrics component : PerformanceMetrics.values() ){
                if( org.apache.commons.lang3.StringUtils.equalsIgnoreCase(id,component.id) ){
                    result = component;
                    break;
                }
            }
            return result;
        }

        public String getDisplay(){
            return display;
        }

        public String getId(){
            return id;
        }
    }

    public PerformanceBarPageObject(WebDriverProvider webDriverProvider){
        super(webDriverProvider);
    }

    @Override
    public By displayLocator(){
        return By.id("performanceTotalsBar");
    }

    @Override
    public WebElement displayElement(){
        return performanceTotalsBar;
    }

    public boolean visibleStateMatches(MetaPageData metaData, Visibility visibility){
        switch( metaData ){
            case TITLE:
                return visibility.isDisplayed() ? elementIsDisplayed(performanceHeaderTitle) : elementIsHidden(
                        displayElement());
            case PERFORMANCE_TOTALS_BAR:
                return visibility.isDisplayed() ? elementIsDisplayed(performanceTotalsBar) : elementIsHidden(
                        displayElement());
            case NEW_WEBSITES_CONTAINER:
                return visibility.isDisplayed() ? elementIsDisplayed(newWebsitesContainer) : elementIsHidden(
                        displayElement());
            case NEW_WEBSITES_TITLE:
                return visibility.isDisplayed() ? elementIsDisplayed(newWebsitesTitle) : elementIsHidden(
                        displayElement());
            case NEW_WEBSITES_TOTAL:
                return visibility.isDisplayed() ? elementIsDisplayed(newWebsitesTotal) : elementIsHidden(
                        displayElement());
            case ACTIVE_WEBSITES_CONTAINER:
                return visibility.isDisplayed() ? elementIsDisplayed(activeWebsitesContainer) : elementIsHidden(
                        displayElement());
            case ACTIVE_WEBSITES_TITLE:
                return visibility.isDisplayed() ? elementIsDisplayed(activeWebsitesTitle) : elementIsHidden(
                        displayElement());
            case ACTIVE_WEBSITES_TOTAL:
                return visibility.isDisplayed() ? elementIsDisplayed(activeWebsitesTotal) : elementIsHidden(
                        displayElement());
            case TOTAL_CLICKS_CONTAINER:
                return visibility.isDisplayed() ? elementIsDisplayed(totalClicksContainer) : elementIsHidden(
                        displayElement());
            case TOTAL_CLICKS_TITLE:
                return visibility.isDisplayed() ? elementIsDisplayed(totalClicksTitle) : elementIsHidden(
                        displayElement());
            case TOTAL_CLICKS_TOTAL:
                return visibility.isDisplayed() ? elementIsDisplayed(totalClicksTotal) : elementIsHidden(
                        displayElement());
            case TOTAL_ACCOUNTS_CONTAINER:
                return visibility.isDisplayed() ? elementIsDisplayed(totalAccountsContainer) : elementIsHidden(
                        displayElement());
            case TOTAL_ACCOUNTS_TITLE:
                return visibility.isDisplayed() ? elementIsDisplayed(totalAccountsTitle) : elementIsHidden(
                        displayElement());
            case TOTAL_ACCOUNTS_TOTAL:
                return visibility.isDisplayed() ? elementIsDisplayed(totalAccountsTotal) : elementIsHidden(
                        displayElement());
        }
        return false;
    }

    public boolean elementTextMatches(MetaPageData metaData, String expected){
        switch( metaData ){
            case TITLE:
                return StringUtils.equalsIgnoreCase(expected,performanceHeaderTitle.getText());
            case NEW_WEBSITES_TITLE:
                return StringUtils.equalsIgnoreCase(expected,newWebsitesTitle.getText());
            case NEW_WEBSITES_TOTAL:
                return StringUtils.equalsIgnoreCase(expected,newWebsitesTotal.getText());
            case ACTIVE_WEBSITES_TITLE:
                return StringUtils.equalsIgnoreCase(expected,activeWebsitesTitle.getText());
            case ACTIVE_WEBSITES_TOTAL:
                return StringUtils.equalsIgnoreCase(expected,activeWebsitesTotal.getText());
            case TOTAL_CLICKS_TITLE:
                return StringUtils.equalsIgnoreCase(expected,totalClicksTitle.getText());
            case TOTAL_CLICKS_TOTAL:
                return StringUtils.equalsIgnoreCase(expected,totalClicksTotal.getText());
            case TOTAL_ACCOUNTS_TITLE:
                return StringUtils.equalsIgnoreCase(expected,totalAccountsTitle.getText());
            case TOTAL_ACCOUNTS_TOTAL:
                return StringUtils.equalsIgnoreCase(expected,totalAccountsTotal.getText());
        }
        return false;
    }

    public boolean performanceMetricTitlesEquals(List<String> expectedList){
        parsePerformanceMetricExpectedTitles();
        return ListUtils.isEqualList(expectedList,actualMetricTitles);
    }

    public boolean performanceMetricsEquals(List<PerformanceMetric> expectedList){
        List<PerformanceMetric> actualMetricList = new Vector<>();
        actualMetricList.add(newMetric(PerformanceMetrics.NEW_WEBSITES));
        actualMetricList.add(newMetric(PerformanceMetrics.ACTIVE_WEBSITES));
        actualMetricList.add(newMetric(PerformanceMetrics.TOTAL_CLICKS));
        actualMetricList.add(newMetric(PerformanceMetrics.TOTAL_ACCOUNTS));

        return ListUtils.isEqualList(expectedList,actualMetricList);
    }

    public boolean performanceMetricValuesEquals(PerformanceIssuerMetrics expectedMetrics){
        parsePerformanceMetricExpectedValues();
        return expectedMetrics.equals(actualPerformanceMetrics);
    }

    private PerformanceMetric newMetric(PerformanceMetrics performanceMetrics){
        String title = null;
        String value = null;
        String image = null;
        WebElement container = null;
        switch( performanceMetrics ){
            case ACTIVE_WEBSITES:
                title = activeWebsitesTitle.getText();
                value = activeWebsitesTotal.getText();
                container = activeWebsitesContainer.findElement(By.className("affrpt_activeweb"));
                break;
            case NEW_WEBSITES:
                title = newWebsitesTitle.getText();
                value = newWebsitesTotal.getText();
                container = newWebsitesContainer.findElement(By.className("affrpt_newweb"));
                break;
            case TOTAL_ACCOUNTS:
                title = totalAccountsTitle.getText();
                value = totalAccountsTotal.getText();
                container = totalAccountsContainer.findElement(By.className("affrpt_tatalacts"));
                break;
            case TOTAL_CLICKS:
                title = totalClicksTitle.getText();
                value = totalClicksTotal.getText();
                container = totalClicksContainer.findElement(By.className("affrpt_totalclk"));
                break;
        }
        String backgroundImage = container.getCssValue("background-image");
        String tmp1 = StringUtils.removeStartIgnoreCase(backgroundImage,"url(");
        String tmp2 = StringUtils.removeEndIgnoreCase(tmp1,")");
        String tmp3 = StringUtils.remove(tmp2,StringCharacter.DOUBLE_QUOTE.toChar());
        String imageName = FilenameUtils.getName(tmp3);
        return new PerformanceMetric(title,value,imageName);
    }

    private void parsePerformanceMetricExpectedTitles(){
        actualMetricTitles = new Vector<>();
        actualMetricTitles.add(PerformanceMetrics.NEW_WEBSITES.getDisplay());
        actualMetricTitles.add(PerformanceMetrics.ACTIVE_WEBSITES.getDisplay());
        actualMetricTitles.add(PerformanceMetrics.TOTAL_CLICKS.getDisplay());
        actualMetricTitles.add(PerformanceMetrics.TOTAL_ACCOUNTS.getDisplay());
    }

    private void parsePerformanceMetricExpectedValues(){
        actualPerformanceMetrics = new PerformanceIssuerMetrics();
        actualPerformanceMetrics.setActiveWebsites(activeWebsitesTotal.getText());
        actualPerformanceMetrics.setNewWebsites(newWebsitesTotal.getText());
        actualPerformanceMetrics.setTotalAccounts(totalAccountsTotal.getText());
        actualPerformanceMetrics.setTotalClicks(totalClicksTotal.getText());
    }
}
