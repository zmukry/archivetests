package com.creditcards.ccx.pages.header;

import com.creditcards.pages.NavigationTab;
import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.By;

public enum MainNavigationTabs implements NavigationTab{
    AFFILIATES("nav_aff_sites","CreditCards.com Affiliates","/ccaffiliates.php?take=1"),
    AFFILIATES_WO_SITES("nav_aff_nosites","Affiliates w/o Sites","/ccaffiliates.php?take=2"),
    CARDS("nav_overview","Cards","/overview.php"),
    CARD_BIDDING_TOOL("nav_card_bidding","Card Bidding Tool","/cpbt/bidding-cycle"),
    REPORTING("nav_aff_reporting","Reporting","/reportDashboard.php"),
    XML_RECIPIENTS("nav_xml","XML Recipients","/recipient.php");

    private final String id;
    private final String name;
    private final String href;
    private final By idSelector;

    private MainNavigationTabs(String id, String name, String href){
        this.id = id;
        this.name = name;
        this.href = href;
        this.idSelector = By.id(id);
    }

    public static MainNavigationTabs valueOfByName(String name){
        MainNavigationTabs result = null;
        for( MainNavigationTabs navigation : MainNavigationTabs.values() ){
            if( StringUtils.equalsIgnoreCase(navigation.name,name) ){
                result = navigation;
                break;
            }
        }
        return result;
    }

    public static MainNavigationTabs valueOfByHref(String href){
        MainNavigationTabs result = null;
        for( MainNavigationTabs navigation : MainNavigationTabs.values() ){
            if( StringUtils.endsWithIgnoreCase(navigation.href,href) ){
                result = navigation;
                break;
            }
        }
        return result;
    }

    public static MainNavigationTabs valueOfById(String id){
        MainNavigationTabs result = null;
        for( MainNavigationTabs navigation : MainNavigationTabs.values() ){
            if( StringUtils.endsWithIgnoreCase(navigation.id,id) ){
                result = navigation;
                break;
            }
        }
        return result;
    }

    public By byIdSelector(){
        return this.idSelector;
    }

    public String getHref(){
        return href;
    }

    public String getId(){
        return id;
    }

    public String getDisplayName(){
        return name;
    }
}
