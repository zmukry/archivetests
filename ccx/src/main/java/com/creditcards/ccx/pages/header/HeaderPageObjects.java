package com.creditcards.ccx.pages.header;

import org.apache.commons.lang3.StringUtils;

public enum HeaderPageObjects{
    HEADER("header","Header"),
    CCX_LOGO("logo-container","CCX Logo"),
    ISSUER_LOGO("issuer-logo","Issuer Logo"),
    WELCOME_MESSAGE("welcome","Welcome Message"),
    MAIN_NAVIGATION("nav-main","Main Navigation Bar");

    private final String id;
    private final String name;

    private HeaderPageObjects(String id, String name){
        this.id = id;
        this.name = name;
    }

    public String getId(){
        return id;
    }

    public String getName(){
        return name;
    }

    public static HeaderPageObjects valueOfByName(String name){
        HeaderPageObjects headerPageObject = null;
        for( HeaderPageObjects page : HeaderPageObjects.values() ){
            if( StringUtils.equalsIgnoreCase(name,page.name) ){
                headerPageObject = page;
                break;
            }
        }
        return headerPageObject;
    }
}
