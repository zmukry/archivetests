package com.creditcards.ccx.pages.header;

import com.creditcards.ccx.utils.Roles;
import com.creditcards.pages.AbstractPageObject;
import org.apache.commons.lang3.StringUtils;
import org.jbehave.web.selenium.WebDriverProvider;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;

public class WelcomePageObject extends AbstractPageObject{

    @FindBy(id = "welcome")
    private WebElement welcomeContainer;

    @FindBys({ @FindBy(id = "welcome"),@FindBy(tagName = "em") })
    private WebElement issuerName;

    @FindBys({ @FindBy(id = "welcome"),@FindBy(tagName = "strong") })
    private WebElement issuerRole;

    @FindBys({ @FindBy(id = "welcome"),@FindBy(tagName = "a") })
    private WebElement logoutLink;

    public WelcomePageObject(WebDriverProvider webDriverProvider){
        super(webDriverProvider);
    }

    @Override
    public By displayLocator(){
        return By.id("welcome");
    }

    @Override
    public WebElement displayElement(){
        return welcomeContainer;
    }

    public void clickLogout(){
        logoutLink.click();
    }

    public boolean userRoleEquals(Roles role){
        return Roles.valueOfByName(issuerRole.getText()) == role;
    }

    public boolean issuerNameEquals(String name){
        return StringUtils.equalsIgnoreCase(name,issuerName.getText());
    }

    public boolean welcomeMessageMatches(String name, String role){
        return StringUtils.equalsIgnoreCase(name,issuerName.getText()) && StringUtils
                .equalsIgnoreCase(role,issuerRole.getText());
    }
}