package com.creditcards.ccx.pages.application;

import org.apache.commons.lang3.StringUtils;

public enum ApplicationPages{
    LOGIN("Login"),
    LOADING("Loading Dialog"),
    OVERVIEW("Overview"),
    REPORT_DASHBOARD("Report Dashboard");

    private final String name;

    ApplicationPages(String pageName){
        this.name = pageName;
    }

    public String getName(){
        return name;
    }

    public static ApplicationPages valueOfByName(String pageName){
        ApplicationPages applicationPage = null;
        for( ApplicationPages page : ApplicationPages.values() ){
            if( StringUtils.equalsIgnoreCase(pageName,page.name) ){
                applicationPage = page;
                break;
            }
        }
        return applicationPage;
    }
}
