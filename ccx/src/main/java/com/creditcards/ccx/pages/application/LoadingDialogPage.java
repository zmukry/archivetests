package com.creditcards.ccx.pages.application;

import com.creditcards.pages.AbstractPageObject;
import org.jbehave.web.selenium.WebDriverProvider;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class LoadingDialogPage extends AbstractPageObject{

    @FindBy(id = "ajaxLoadPanelHtml")
    private WebElement loadingPanel;

    public LoadingDialogPage(WebDriverProvider webDriverProvider){
        super(webDriverProvider);
    }

    @Override
    public WebElement displayElement(){
        return loadingPanel;
    }

    @Override
    public By displayLocator(){
        return By.id("ajaxLoadPanelHtml");
    }
}