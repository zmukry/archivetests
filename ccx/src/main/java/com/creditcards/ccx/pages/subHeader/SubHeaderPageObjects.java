package com.creditcards.ccx.pages.subHeader;

import org.apache.commons.lang3.StringUtils;

public enum SubHeaderPageObjects{
    SUB_HEADER("subhead","Page Sub Header"),
    CARD_NAVIGATION("nav","Card Navigation");

    private final String id;
    private final String name;

    private SubHeaderPageObjects(String id, String name){
        this.id = id;
        this.name = name;
    }

    public String getId(){
        return id;
    }

    public String getName(){
        return name;
    }

    public static SubHeaderPageObjects valueOfByName(String name){
        SubHeaderPageObjects subHeaderPageObject = null;
        for( SubHeaderPageObjects page : SubHeaderPageObjects.values() ){
            if( StringUtils.equalsIgnoreCase(name,page.name) ){
                subHeaderPageObject = page;
                break;
            }
        }
        return subHeaderPageObject;
    }
}
