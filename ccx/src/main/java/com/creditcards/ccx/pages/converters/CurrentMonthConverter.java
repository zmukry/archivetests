package com.creditcards.ccx.pages.converters;

import org.apache.commons.lang3.StringUtils;
import org.jbehave.core.steps.ParameterConverters.ParameterConverter;
import org.joda.time.DateTime;

import java.lang.reflect.Type;
import java.util.Calendar;

public class CurrentMonthConverter implements ParameterConverter{

    private final static DateTime.Property currentMonth = new DateTime().monthOfYear();

    public CurrentMonthConverter(){
        Calendar.getInstance().get(Calendar.MONTH);
    }

    public boolean accept(Type type){
        return type instanceof Class<?> && Calendar.class.isAssignableFrom((Class<?>)type);
    }

    public Object convertValue(String value, Type type){
        return ( StringUtils.isBlank(value) || "none".equals(value) ) ? null : currentMonth;
    }
}
