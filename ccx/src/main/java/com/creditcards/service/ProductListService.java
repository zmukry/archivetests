package com.creditcards.service;

import com.creditcards.model.Product;

import java.util.Map;

public class ProductListService{
    private MessageService messageService;
    private TestDomain testDomain;

    public ProductListService(MessageService messageService){
        this.messageService = messageService;
    }

    protected ProductListService(){
    }

    public Product newTransaction(Map<String, String> productDetails){
        Product transaction = null;
        switch( testDomain ){
            case DEVELOPMENT:
                break;
            case QA:
                break;
            case PRODUCTION:
                break;
        }
        return transaction;
    }

    public String valueOf(String sku){
        return messageService.valueOf(sku);
    }
}