package com.creditcards.service;

public class ProductListServiceBootstrap{

    private MessageService messageServiceConfiguration;

    public ProductListService productListService(){
        return new ProductListService(messageServiceConfiguration);
    }
}