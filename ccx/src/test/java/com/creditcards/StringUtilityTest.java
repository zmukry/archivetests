package com.creditcards;

import com.creditcards.lang.StringCharacter;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.text.StrBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

public class StringUtilityTest{
    private final static Logger logger = LoggerFactory.getLogger(StringUtilityTest.class);

    @Test
    public void verifyDates() throws Exception{
        String d1 = "Apr 20, 2013";
    }

    public void testStringParsing(){
        StrBuilder builder = new StrBuilder();
        builder.append("url").append(StringCharacter.LEFT_PAREN).append(StringCharacter.DOUBLE_QUOTE)
               .append("http://ccx.ci.creditcards.com/etc/images/report_dashboard/totalclicks.png")
               .append(StringCharacter.DOUBLE_QUOTE).append(StringCharacter.RIGHT_PAREN);

        String tmp1 = StringUtils.removeStartIgnoreCase(builder.toString(),"url(");
        String tmp2 = StringUtils.removeEndIgnoreCase(tmp1,")");
        String tmp3 = StringUtils.remove(tmp2,StringCharacter.DOUBLE_QUOTE.toChar());
        String actual = FilenameUtils.getName(tmp3);
        String expected = "totalclicks.png";
        assertEquals(actual,expected);
    }
}
