package com.creditcards.ccx.pages.reporting;

import com.creditcards.cache.service.CacheService;
import com.creditcards.ccx.pages.PageManager;
import com.creditcards.pages.PageObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

@ContextConfiguration(locations = "classpath:applicationContext-test.xml")
public class ReportPageManagerTest extends AbstractTestNGSpringContextTests{
    private static final Logger logger = LoggerFactory.getLogger(ReportPageManagerTest.class);

    @Autowired
    private PageManager pageManager;

    @Autowired
    private CacheService cacheService;

    @Test
    public void verifyReportPageCaching(){
        for( PerformanceReports performanceReports : PerformanceReports.values() ){
            assertFalse(cacheService.containsKey(performanceReports));

            PageObject page = pageManager.getPerformanceReportPageObject(performanceReports);
            assertTrue(cacheService.containsKey(performanceReports));
            assertEquals(page,page);

            PageObject page2 = pageManager.getPerformanceReportPageObject(performanceReports);
            assertEquals(page,page2);
        }
    }
}
