# Etsy.com using Java and Spring

This tutorial uses JBehave 3.x and Selenium 2.x to test [Etsy.com](http://etsy.com) (an live online shopping site).

<img src="http://jbehave.org/reference/preview/images/jbehave-logo.png" alt="JBehave logo" align="right" />

## Running the stories

This will run the build and (after a minute or so) Firefox will open and test the etsy.com website:

    mvn install

You should see Firefox (installed on your system) flicker as it tests Etsy.com

This will run a single story (one contained in a etsy_cart.story file):

    mvn install -DstoryFilter=etsy_cart

This will run a suite based on the meta filters in the three story files:

    mvn install -Dmeta.filter="+color red"

## Viewing the results

In a directory target/view, a page named 'reports.html' has been generated.
If you open that in any brower you can see the stories that have run and their execution status.

There should be a row for each story.  The story reports are clickable to via links on the right-most column.

## Using this tutorial to start your own JBehave-based integration tests for a web site.

The tutorial aims to provide a fully-functional project that you can use to model you own project:

1. src/main/java/org/jbehave/tutorials/etsy/EtsyDotComStories.java is the entry-point that JBehave uses to run the stories.
2. src/main/stories contains the stories run by JBehave via EtsyDotComStories.java.
3. src/main/java/org/jbehave/tutorials/etsy/steps/HousekeepingSteps.java contains the steps does housekeeping chores, such as emptying cart between scenarios.
4. src/main/java/org/jbehave/tutorials/etsy/steps/EtsyDotComSteps.java contains the steps mapped to the textual steps.
5. src/main/java/org/jbehave/tutorials/etsy/pages contains the Groovy page-objects used by steps to abstract in a more manageable and maintainable way the interaction with the web pages via Selenium WebDriver.
6. src/main/resources/etsy-steps.xml contains the Spring configuration for composition the steps

Maven Transitive Dependencies
JBehave and spring are the primary technologys.
The jbehave-spring module,jbehave-web-selenium, selenium-java are defined as the parent modules,
hence defining the transitive dependencies.

(jbehave-spring) artifact depends on ...
Group	            Artifact	    Version
cglib	            cglib	        2.2.2
org.jbehave	        jbehave-core	3.9.1
org.springframework	spring-context	3.1.1.RELEASE
org.springframework	spring-test	    3.1.1.RELEASE

(jbehave-Core) artifact depends on ...
Group	                    Artifact	Version
com.google.guava	        guava	            14.0
com.thoughtworks.paranamer	paranamer	        2.4
com.thoughtworks.xstream	xstream	            1.4.5
commons-collections	        commons-collections	3.2.1
commons-io	                commons-io	        2.4
commons-lang	            commons-lang	    2.6
javax.inject	            javax.inject	    1
junit	                    junit	            4.11
org.codehaus.groovy	        groovy-all	        2.1.1
org.codehaus.plexus	        plexus-utils	    3.0.10
org.freemarker	            freemarker	        2.3.19
org.hamcrest	            hamcrest-core	    1.3
org.hamcrest	            hamcrest-integration	1.3
org.hamcrest	            hamcrest-library	1.3
xmlunit	                    xmlunit	            1.4


(jbehave-web-selenium)artifact depends on ...
Group	                        Artifact	    Version
com.github.tanob	            groobe	        1.2
com.google.guava	            guava	        11.0.1
org.codehaus.geb	            geb-core	    0.7.0
org.codehaus.groovy	            groovy-all	    1.8.6
org.freemarker	                freemarker	    2.3.19
org.picocontainer	            picocontainer	2.14.1
org.seleniumhq.selenium.fluent	fluent-selenium	1.6.3
org.seleniumhq.selenium	        selenium-java	2.26.0

(selenium-server)artifact depends on ...
Group	                Artifact	    Version
commons-codec	        commons-codec
junit	                junit	        4.9
mx4j	                mx4j-tools	    3.0.1
net.jcip	            jcip-annotations
org.bouncycastle	    bcpkix-jdk15on	1.48
org.bouncycastle	    bcprov-jdk15on	1.48
org.mortbay.jetty	    servlet-api-2.5	6.1.9
org.seleniumhq.selenium	jetty-repacked	7.6.1
org.seleniumhq.selenium	selenium-java	2.40.0
org.yaml	            snakeyaml 	    1.8

