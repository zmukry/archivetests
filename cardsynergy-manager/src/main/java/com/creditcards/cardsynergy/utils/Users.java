package com.creditcards.cardsynergy.utils;

import org.apache.commons.lang3.StringUtils;

/**
 * Copyright (c) 2013-06-04, CreditCards.com. All Rights Reserved.
 * User: jeffreyhynes
 * Date: 2013-06-04
 * com.creditcards.cardsynergy.utils
 */
public enum Users{
    AFFILIATE("QA_System_Admin","cards"),
    CESAR("cesarg","foo"),
    SUPERMAN("cesarg","foo");

    private final String username;
    private final String password;

    private Users(String password, String username){
        this.password = password;
        this.username = username;
    }

    public String getPassword(){
        return password;
    }

    public String getUsername(){
        return username;
    }
    public static Users valueOfByName(String name) {
        Users user = null;
        for (Users userType : Users.values()) {
            if (StringUtils.equalsIgnoreCase(name,userType.username)) {
                user = userType;
                break;
            }
        }
        return user;
    }
}
