package com.creditcards.cccomus.bdd.steps;

import com.creditcards.steps.Steps;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.jbehave.web.selenium.WebDriverProvider;
import org.springframework.beans.factory.annotation.Autowired;

@Steps
public class PleaseWaitSteps{

    private final WebDriverProvider webDriverProvider;

    @Autowired
    public PleaseWaitSteps(WebDriverProvider webDriverProvider){
        this.webDriverProvider = webDriverProvider;
    }

    @When("the Please wait pages is hidden")
    public void pageIsHidden(){

    }

    @Then("the Please wait pages is displayed")
    public void pageIsDisplayed(){

    }
}
