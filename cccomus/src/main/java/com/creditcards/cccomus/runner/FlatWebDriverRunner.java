package com.creditcards.cccomus.runner;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.Test;

import static org.testng.Assert.assertTrue;

public class FlatWebDriverRunner{
    private final static Logger logger = LoggerFactory.getLogger(FlatWebDriverRunner.class);

    @Test
    public void executeCreditScoreEstimator(){
        // Create a new instance of the Firefox driver
        // Notice that the remainder of the code relies on the interface,
        // not the implementation.
        logger.info("Create a new instance of the Firefox driver");
        final WebDriver driver = new FirefoxDriver();

        driver.get("http://www.creditcards.com");

        //Verify the pages is really open?
        /*
            Icon is displayed
            Main pages container is displayed
         ...
         */
        logger.info("Verify the pages is really open?");

        WebElement topbar = driver.findElement(By.tagName("table").className("topbar"));
        assertTrue(topbar.isDisplayed());

        logger.info("Icon is displayed");
        WebElement logo = topbar.findElement(By.tagName("img"));
        assertTrue(logo.isDisplayed());

        logger.info("Icon is displayed Using css Selector");
        WebElement logoCss = topbar.findElement(By.cssSelector("img[alt='CreditCards.com']"));
        assertTrue(logoCss.isDisplayed());

        logger.info("Click the Not Sure Search Link");
        WebElement leftnav = driver.findElement(By.id("leftnav"));
        assertTrue(leftnav.isDisplayed());

        WebElement notSureLink = leftnav.findElement(By.linkText("Not Sure?"));
        assertTrue(notSureLink.isDisplayed());
        notSureLink.click();

        logger.info("Wait for the pages to load, timeout after 10 seconds");
        //Version 1
        final By locator = By.cssSelector("td.mainContentColumn");

        (new WebDriverWait(driver,10)).until(new ExpectedCondition<Boolean>(){
            public Boolean apply(WebDriver d){
                WebElement toReturn = driver.findElement(locator);
                return toReturn.isDisplayed();
            }
        });

        //Version 2
        Wait<WebDriver> wait = new WebDriverWait(driver,30);
        wait.until(visibilityOfElementLocated(locator));

        logger.info("Select Question 1");
        Select q1 = new Select(driver.findElement(By.id("q1_id")));
        q1.selectByValue("C");

        logger.info("Wait for Question 1a to display");
        wait.until(visibilityOfElementLocated(By.id("q1a_div_id")));

        logger.info("Select Question 1a");
        WebElement q1aElement = driver.findElement(By.id("q1a_div_id")).findElement(By.className("estimateselect"));
        Select q1a = new Select(q1aElement);
        q1a.selectByValue("H");
        //....
        //Close the browser
        // driver.quit();
    }

    public ExpectedCondition<WebElement> visibilityOfElementLocated(final By locator){
        return new ExpectedCondition<WebElement>(){
            public WebElement apply(WebDriver driver){
                WebElement toReturn = driver.findElement(locator);
                if(toReturn.isDisplayed()){
                    return toReturn;
                }
                return null;
            }
        };
    }
}
