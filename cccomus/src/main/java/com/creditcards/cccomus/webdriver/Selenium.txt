WebDriver is a tool for automating web application testing, and in particular to verify that they work as expected.


Being a smart guy, he realized there were better uses of his time than manually stepping through the same tests with every change he made.
Selenium started as a Javascript library that could drive interactions with the page, allowing him to automatically rerun tests against multiple browsers
A sandboxed Javascript environment.

2.0
Webdriver now interacts directly to the browser using the ‘native’ method for the browser and operating system.

Selenium-Grid
Remote invocation of Functional tests.

Programming languages
C#
.net
Perl
PHP
Java
Python
Ruby

Specific Drivers
Firefox, Flash, Chrome Driver, Opera Driver, Android Driver and iPhone Driver

Locating UI Elements (WebElements)
The webdriver  expose bindings for “Find Element” and “Find Elements”

The “Find” methods take a locator or query object called “By”.
Mechanism used to locate elements within a document.

BY Id
By Class Name
By Tag Name
By Link Text
By Partial Link Text
By CSS
By XPATH

http://sauceio.com/index.php/2011/05/why-css-locators-are-the-way-to-go-vs-xpath/


--- DEMO 1: FLAT


