package com.creditcards.cccomus.webdriver.pages;

import com.google.common.base.Function;
import org.jbehave.web.selenium.WebDriverPage;
import org.jbehave.web.selenium.WebDriverProvider;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public abstract class AbstractWebPage extends WebDriverPage implements PageObject{
    protected AbstractWebPage(WebDriverProvider driverProvider){
        super(driverProvider);
    }

    public void initElements(){
        int DRIVER_WAIT = 5;
        PageFactory.initElements(new AjaxElementLocatorFactory(getDriverProvider().get(),DRIVER_WAIT),this);
    }

    public boolean isDisplayed(){
        return elementIsDisplayed(displayElement());
    }

    public boolean isHidden(){
        return !isDisplayed();
    }

    public void waitUntilLoaded(){
        initElements();
        new WebDriverWait(getDriverProvider().get(),20)
                .until(refreshed(ExpectedConditions.visibilityOf(displayElement())));
    }

    public void waitUntilLoaded(WebElement displayElement){
        new WebDriverWait(getDriverProvider().get(),20)
                .until(refreshed(ExpectedConditions.visibilityOf(displayElement)));
    }

    public boolean elementIsDisplayed(WebElement element){
        boolean result;
        try{
            result = null != element && element.isDisplayed();
        }catch(NoSuchElementException e){
            result = false;
        }catch(StaleElementReferenceException staleException){
            result = false;
        }
        return result;
    }

    public boolean elementIsHidden(WebElement element){
        return !elementIsDisplayed(element);
    }

    private <T> ExpectedCondition<T> displayed(final Function<WebDriver, T> originalFunction){
        return new ExpectedCondition<T>(){
            @Override
            public T apply(WebDriver webdriver){
                try{
                    return originalFunction.apply(webdriver);
                }catch(StaleElementReferenceException sere){
                    throw new NoSuchElementException("Element stale.",sere);
                }
            }
        };
    }

    private <T> ExpectedCondition<T> refreshed(final Function<WebDriver, T> originalFunction){
        return new ExpectedCondition<T>(){
            @Override
            public T apply(WebDriver webdriver){
                try{
                    return originalFunction.apply(webdriver);
                }catch(StaleElementReferenceException sere){
                    throw new NoSuchElementException("Element stale.",sere);
                }
            }
        };
    }
}
