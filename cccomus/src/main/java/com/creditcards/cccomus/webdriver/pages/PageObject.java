package com.creditcards.cccomus.webdriver.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public interface PageObject{
    void initElements();

    void waitUntilLoaded();

    By displayLocator();

    WebElement displayElement();

    boolean isDisplayed();

    boolean isHidden();
}
