package com.creditcards.cccomus.webdriver.pages;

import org.apache.commons.lang3.StringUtils;
import org.jbehave.web.selenium.WebDriverProvider;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import org.openqa.selenium.support.ui.Select;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static org.apache.commons.lang3.StringUtils.EMPTY;
import static org.testng.Assert.assertEquals;

public class CreditScoreEstimatorPage extends AbstractWebPage implements PageObject{
    private final static Logger logger = LoggerFactory.getLogger(CreditScoreEstimatorPage.class);

    @FindBys({@FindBy(id = "skeleton"),@FindBy(css = "table:nth-child(2) td.mainContentColumn")})
    private WebElement mainContentColumn;

    @FindBy(id = "pageContentArea")
    private WebElement pageContentArea;

    @FindBy(id = "breadcrumb")
    private WebElement breadcrumb;

    @FindBy(id = "q1_id")
    private WebElement question1;

    @FindBys({@FindBy(id = "q1a_div_id"),@FindBy(tagName = "select")})
    private WebElement question1a;

    @FindBys({@FindBy(id = "pageContentArea"),@FindBy(name = "q2")})
    private WebElement question2;

    @FindBys({@FindBy(id = "pageContentArea"),@FindBy(css = "select[name='q3']")})
    private WebElement question3;

    @FindBys({@FindBy(id = "pageContentArea"),@FindBy(name = "q4")})
    private WebElement question4;

    @FindBys({@FindBy(id = "pageContentArea"),@FindBy(name = "q5")})
    private WebElement question5;

    @FindBys({@FindBy(id = "pageContentArea"),@FindBy(name = "q6")})
    private WebElement question6;

    @FindBy(id = "q7_id")
    private WebElement question7;

    @FindBys({@FindBy(id = "q7a_div_id"),@FindBy(tagName = "select")})
    private WebElement question7a;

    @FindBy(id = "q8_id")
    private WebElement question8;

    @FindBys({@FindBy(id = "q8a_div_id"),@FindBy(tagName = "select")})
    private WebElement question8a;

    @FindBys({@FindBy(id = "pageContentArea"),@FindBy(name = "q9")})
    private WebElement question9;

    @FindBys({@FindBy(id = "pageContentArea"),@FindBy(name = "q10")})
    private WebElement question10;

    @FindBys({@FindBy(id = "pageContentArea"),@FindBy(css = "input[name='login']")})
    private WebElement viewResultsButton;

    @FindBys({@FindBy(id = "pageContentArea"),@FindBy(css = "table td:last-child")})
    private WebElement pageHeader;

    public enum Question1{
        DEFAULT(EMPTY,"---- Select one ----"),
        NO_OPEN_CARDS("A","I have no open credit cards"),
        ONE("B","1"),
        TWO_TO_FOUR("C","2 to 4"),
        FIVE_TO_EIGHT("D","5 to 8"),
        EIGHT_TO_TWELVE("D","8 to 12"),
        MORE_THAN_TWELVE("D","more than 12");

        private final String value;
        private final String displayValue;

        private Question1(String value, String displayValue){
            this.displayValue = displayValue;
            this.value = value;
        }

        public static Question1 valueOfByDisplayValue(String displayValue){
            Question1 question = null;
            for(Question1 option : Question1.values()){
                if(StringUtils.equalsIgnoreCase(displayValue,option.displayValue)){
                    question = option;
                    break;
                }
            }
            return question;
        }
    }

    public CreditScoreEstimatorPage(WebDriverProvider driverProvider){
        super(driverProvider);
    }

    public boolean isBreadCrumbDisplayed(String expected){
        return elementIsDisplayed(breadcrumb) && StringUtils.contains(breadcrumb.getText(),expected);
    }

    public boolean headerEquals(String title, String content){
        WebElement headerTitle = pageHeader.findElement(By.tagName("h1"));
        WebElement headerContent = pageHeader.findElement(By.tagName("p"));
        return pageHeader.isDisplayed() && StringUtils.contains(headerTitle.getText(),title) &&
                StringUtils.equalsIgnoreCase(content,headerContent.getText());
    }

    public void selectQuestion1(Question1 option){
        logger.info("Selecting question 1 option:" + option.displayValue);
        Select select = new Select(question1);
        select.selectByValue(option.value);

        logger.info("Asserting the selected option was really selected.");
        String actualValue = select.getFirstSelectedOption().getText();
        assertEquals(Question1.valueOfByDisplayValue(actualValue),option);
    }

    public boolean isQuestion1aDisplayed(){
        return elementIsDisplayed(question1a);
    }

    public void selectQuestion1a(String option){
        logger.info("Selecting question 1a option:" + option);
        Select select = new Select(question1a);
        select.selectByVisibleText(option);
    }

    public void question1aEquals(String expected){
        logger.info("Asserting the selected option was really selected.");
        Select select = new Select(question1a);
        String actualValue = select.getFirstSelectedOption().getText();
        assertEquals(actualValue,expected);
    }

    public void selectQuestion2(String option){
        logger.info("Selecting question 2 option:" + option);
        Select select = new Select(question2);
        select.selectByVisibleText(option);
    }

    public void selectQuestion3(String option){
        logger.info("Selecting question 3 option:" + option);
        Select select = new Select(question3);
        select.selectByVisibleText(option);
    }

    public void selectQuestion4(String option){
        logger.info("Selecting question 4 option:" + option);
        Select select = new Select(question4);
        select.selectByVisibleText(option);
    }

    public void selectQuestion5(String option){
        logger.info("Selecting question 5 option:" + option);
        Select select = new Select(question5);
        select.selectByVisibleText(option);
    }

    public void selectQuestion6(String option){
        logger.info("Selecting question 6 option:" + option);
        Select select = new Select(question6);
        select.selectByVisibleText(option);
    }

    public void selectQuestion7(String option){
        logger.info("Selecting question 7 option:" + option);
        Select select = new Select(question7);
        select.selectByVisibleText(option);
    }

    public void selectQuestion8(String option){
        logger.info("Selecting question 8 option:" + option);
        Select select = new Select(question8);
        select.selectByVisibleText(option);
    }

    public void selectQuestion8a(String option){
        logger.info("Selecting question 8a option:" + option);
        Select select = new Select(question8a);
        select.selectByVisibleText(option);
    }

    public void selectQuestion9(String option){
        logger.info("Selecting question 9 option:" + option);
        Select select = new Select(question9);
        select.selectByVisibleText(option);
    }

    public void selectQuestion10(String option){
        logger.info("Selecting question 10 option:" + option);
        Select select = new Select(question10);
        select.selectByVisibleText(option);
    }

    public void clickViewResults(){
        viewResultsButton.click();
    }

    @Override
    public WebElement displayElement(){
        return mainContentColumn;
    }

    @Override
    public By displayLocator(){
        return By.cssSelector("td.mainContentColumn");
    }
}
